alter table c_productos_wdsl
	add descripcion varchar(4000) null;

alter table c_productos_wdsl
	add url_img varchar(200) null;



alter table c_productos_wdsl
	add precio int null after url_img;

alter table c_productos_wdsl modify registro timestamp default CURRENT_TIMESTAMP null on update CURRENT_TIMESTAMP after precio;



alter table c_productos_wdsl
	add id_wp int null after precio;



alter table c_categorias_wdsl
	add id_wp int null;


alter table c_categorias_wdsl
	add estado int null;

    alter table c_categorias_wdsl alter column id_wp set default -1;


    create unique index c_categorias_wdsl_NameCategory_uindex
	on c_categorias_wdsl (NameCategory);


alter table c_productos_wdsl
	add estado int null;


    alter table c_productos_wdsl alter column estado set default 0;



alter table c_productos_wdsl alter column id_wp set default -1;

TRUNCATE c_productos_wdsl;
TRUNCATE c_productos_categorias;
TRUNCATE c_categorias_wdsl;
