<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Payucontroller extends CI_Controller {

	function __construct() {

		parent::__construct();
		$this->load->helper('url');
		$this->load->library('payuliberary');
		$this->load->model('models_cupones');
		$this->load->model('models_historial');
		$this->load->model('models_ventas_wsdl');
		$this->load->model('models_facturacion');
		$this->load->library('session');
		$sessionId = $this->session->userdata('sessionId');
		if (strlen($sessionId) == 0) {
			redirect('', 'refresh');
		}
	}

	

	public function emailfinal()
	{	
		$idfacturacion=$this->input->get('idfacturacion');

		$query=$this->models_facturacion->getbyid($idfacturacion);		
		$row = $query->row();
		$data['info'] =$row;
		$data['nombre'] =$idfacturacion;
		$data['registros'] =$this->models_ventas_wsdl->getVentasfac($idfacturacion);

		$html=$this->load->view('sitio/emailcomprea',$data,true);
	
		echo $html;
		

	}



}

?>