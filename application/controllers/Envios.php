<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Envios extends CI_Controller {
	private $limite = 10;

	function __construct() {

		parent::__construct();
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->library('pagination');
		$this->load->library('upload');
		$this->load->model('servicios/envios_servicios','as');
		$this->load->model('servicios/admin_servicios','asLogin');
		$sessionId = $this->session->userdata('sessionId');

		if (strlen($sessionId) == 0) {
			redirect('', 'refresh');
		}

	}

	public function crearArchivo()
	{
		$this->load->helper('file');
		//$phat=$_SERVER['DOCUMENT_ROOT'].;
		//echo URL_UPLOAD;

		$regiones=$this->asLogin->getRegiones();

		$data ='id_Region,nombre_region,id_comuna,nombre_comuna,precio,precio_extra'."\n";
		if (isset($regiones)) {
			foreach($regiones as $cat){

				if(!empty($cat->idState)){

					$id=$cat->idState;
					$nomregion=$cat->state_name;
					$comunas=$this->asLogin->getComunas($id);
					if (isset($comunas)) {
						foreach($comunas as $catb)
						{

							if(!empty($catb->idProvince)){
								$idcomuna=$catb->idProvince;
								$nomcomuna=$catb->procince_name;

								$data.= $id.','.$nomregion.','.$idcomuna.','.$nomcomuna.',0,0'."\n";


							}

						}
					}
				}

			}
		}

		//cho $data;
		
		if ( ! write_file( URL_UPLOAD.'CargaMasiva.csv', $data))
		{
			echo 'Cd';
		}
		else
		{
			echo 'File written!';
		}

		



	}

	public function guardarcampCategoria()
	{

		$id = $this->input->post('id');
		$productosOcategorias = $this->input->post('categorias');

		

		$this->as->deleteCampaniaCate($id);
		

		if (isset($productosOcategorias)) {

			foreach($productosOcategorias as $selected){
				$parametros = array(
					"idcampania" => $id,
					"IdCategory" => $selected);
				$this->as->insertarCampaniaCate($parametros);
			}
		}
		redirect('envios/capturaCategoria?id='.$id, 'refresh');



	}

	public function capturaCategoria()
	{
		$idcampania=$this->input->get('id');
		$data['id'] =$idcampania;
		$data['categorias'] =$this->as->getCategoriaCampania($idcampania);
		$data['topbar'] = $this->load->view('plantilla/topbar','', true);
		$data['menu'] = $this->load->view('plantilla/menu','', true);
		$this->load->view('envios/categoria',$data);

	}


	public function productosCate()
	{
		$id = $this->input->get('IdCategory');


		$hm='';



		$resultado=$this->as->getProductosCampania($id,$this->input->get('id'));

		if (isset($resultado)) {
			foreach ($resultado->result() as $rowx) {

				$validar=$rowx->validar;

				if($validar!=0){
					$hm.='<option value="'.$rowx->ProductReference.'" selected>'.$rowx->Name.'</option>';
				}else{
					$hm.='<option value="'.$rowx->ProductReference.'">'.$rowx->Name.'</option>';
				}
			}
		}


		echo $hm;
	}

	public function guardarcampProductos()
	{

		$id = $this->input->post('id');
		$categoria = $this->input->post('categoria');
		
		$productosOcategorias = $this->input->post('productosOcategorias');

		if($categoria!=-1){

			$this->as->deleteCampaniaProductid($id,$categoria);

		}else{

			$this->as->deleteCampaniaProduct($id);
		}

		if (isset($productosOcategorias)) {

			foreach($productosOcategorias as $selected){
				$parametros = array(
					"idcampania" => $id,
					"ProductReference" => $selected);
				$this->as->insertarCampaniaProduct($parametros);
			}
		}
		redirect('envios/capturaProductos?id='.$id, 'refresh');



	}

	public function capturaProductos()
	{
		
		$data['resultado'] =$this->as->getCategorias();
		$data['id'] =$this->input->get('id');
		$data['productos'] =$this->as->getProductosCampania(-1,$this->input->get('id'));
		$data['topbar'] = $this->load->view('plantilla/topbar','', true);
		$data['menu'] = $this->load->view('plantilla/menu','', true);
		$this->load->view('envios/productos',$data);

	}

	public function validarArchivo()
	{

		$id = $this->input->post('id');
		$filename=$_FILES["file"]["name"];
		$info = new SplFileInfo($filename);

		$extension = pathinfo($info->getFilename(), PATHINFO_EXTENSION);

		$previo=array();
		if($extension == 'csv')
		{    
			$filename = $_FILES['file']['tmp_name'];


			$handle = fopen($filename, "r");

			

			while( ($data = fgetcsv($handle, 1000, ",") ) !== FALSE )
			{
				//echo $data[0];
				
				$id_comuna="ERROR";
				$precio="ERROR";
				$id_Region="ERROR";
				$nombre_region="ERROR";
				$nombre_comuna="ERROR";
				$precio_extra="ERROR";
				$error=0;

				

				if(is_numeric($data[0])) {
					$id_Region=$data[0];

				}else{
					$id_Region="ERROR";
					$error=1;
				}

				
				$nombre_region=$data[1];

				


				if(is_numeric($data[2])) {
					$id_comuna=$data[2];

				}else{
					$id_comuna="ERROR";
					$error=1;
				}

				
				$nombre_comuna=$data[3];

				

				if(is_numeric($data[4])) {
					$precio=$data[4];

				}else{
					$precio="ERROR";
					$error=1;
				}

				if(is_numeric($data[5])) {
					$precio_extra=$data[5];

				}else{
					$precio_extra="ERROR";
					$error=1;
				}

				// echo $data[2]." 2 <br>";
				// echo $data[5]." 5 <br>";
				// echo $data[4]." 4 <br>";

				if(is_numeric($data[5])&&is_numeric($data[4])) {

					if($data[5]!=0||$data[4]!=0) {
						$objcate = array(
							'id_Region'=>$id_Region,
							'nombre_region'=>$nombre_region,
							'id_comuna'=>$id_comuna,
							'nombre_comuna'=>$nombre_comuna,
							'precio'=>$precio,
							'error'=>$error,
							'precio_extra'=>$precio_extra);

//print_r($objcate);


						array_push($previo,$objcate);
					}
					//break;
				}



			}

			
		}

		//echo $data[0];

		$prodcateaux = array_unique($previo, SORT_REGULAR);

		//print_r($prodcateaux);
		$data['idcampania']=$id;
		
		$data['prodcateaux']=$prodcateaux;
		$datosSesion = array(
			'cargarjson' => json_encode($prodcateaux)
		);
		$this->session->set_userdata($datosSesion);
		$data['topbar'] = $this->load->view('plantilla/topbar','', true);
		$data['menu'] = $this->load->view('plantilla/menu','', true);
		$this->load->view('envios/cargaMasiva',$data);


	}
	public function guardarMasiva()
	{

		$id = $this->input->post('id');
		$cargarjson = $this->session->userdata('cargarjson');
		$data = json_decode($cargarjson, TRUE);

		for($i=0;$i<count($data);$i++){

			if($data[$i]["error"]!=1){

				/*echo $data[$i]["id_Region"]." - ";
				echo $data[$i]["id_comuna"]."- ";
				echo $data[$i]["precio"]."- ";
				echo $data[$i]["precio_extra"]."<br>";*/

				$objcate = array('idProvince'=>$data[$i]["id_comuna"],
					'procince_name'=>$data[$i]["nombre_comuna"],
					'idState'=>$data[$i]["id_Region"],
					'id'=>$id,
					'precio'=>$data[$i]["precio"],
					'precio_extra'=>$data[$i]["precio_extra"],
					'state_name'=>$data[$i]["nombre_region"]);

				$valor=$this->as->vertificaComuna($data[$i]["id_comuna"],$data[$i]["id_Region"],$id);
				if($valor==0){
					$this->as->insertarregionCampania($objcate);
				}
			}
		}

		redirect('envios/providencias?id='.$id, 'refresh');




	}


	public function cragaMasiva()
	{

		$id = $this->input->get('id');
		$data['idcampania']=$id;
		$data['topbar'] = $this->load->view('plantilla/topbar','', true);
		$data['menu'] = $this->load->view('plantilla/menu','', true);
		$this->load->view('envios/cargaMasiva',$data);


	}

	public function elimiarProvi()
	{

		$hobby  = $this->input->post('ideliminar');
		$idcampania  = $this->input->post('idcampania');
		foreach ($hobby as $hobys=>$value) {



			$this->as->deleteregion($value);
		}

		redirect('envios/providencias?id='.$idcampania, 'refresh');


	}

	public function udateprecioExtra()
	{

		$id = $this->input->get('id');
		$valor = $this->input->get('valor');

		$objcate = array(
			'precio_extra'=>$valor);

		$this->as->updateregion($id,$objcate);



	}

	public function udateCatidad()
	{

		$id = $this->input->get('id');
		$valor = $this->input->get('valor');

		$objcate = array(
			'precio'=>$valor);

		$this->as->updateregion($id,$objcate);

	}


	public function agergarProvidencias()
	{

		$idcampania = $this->input->get('idcampania');
		$idregiones = $this->input->get('idregiones');
		$idcomunas = $this->input->get('idcomunas');
		$nomregion = $this->input->get('nomregion');
		$dnomcomunas = $this->input->get('dnomcomunas');

		if($idcomunas!=-1){

			$objcate = array('idProvince'=>$idcomunas,
				'procince_name'=>$dnomcomunas,
				'idState'=>$idregiones,
				'id'=>$idcampania,
				'precio'=>0,
				'precio_extra'=>0,
				'state_name'=>$nomregion);

			$valor=$this->as->vertificaComuna($idcomunas,$idregiones,$idcampania);

			if($valor==0){

				$this->as->insertarregionCampania($objcate);
			}
		}/*else{

			$comunas=$this->asLogin->getComunas($idregiones);

			foreach($comunas as $cat)
			{

				$objcate = array('idProvince'=>$cat->idProvince,
					'procince_name'=>$cat->procince_name,
					'idState'=>$idregiones,
					'id'=>$idcampania,
					'ip'=>$this->input->ip_address(),
					'state_name'=>$nomregion);

				$this->as->insertarregionCampania($objcate);

				
			}


		}*/

		


       //redirect('envios/providencias?id='.$idcampania, 'refresh');

	}

	public function regiones()
	{

		$id = $this->input->get('id');
		$regiones=$this->asLogin->getRegiones();
		$data['regiones']=$regiones;
		$data['idcampania']=$id;
		$data['topbar'] = $this->load->view('plantilla/topbar','', true);
		$data['menu'] = $this->load->view('plantilla/menu','', true);
		$this->load->view('envios/providenciasCatalogo',$data);


	}

	public function providencias()
	{

		$id = $this->input->get('id');
		$regiones=$this->asLogin->getRegiones();
		$sProvince=$this->as->getProvidencias($this->input->ip_address(),$id);
		$data['sProvince']=$sProvince;
		$data['regiones']=$regiones;
		$data['idcampania']=$id;
		$data['topbar'] = $this->load->view('plantilla/topbar','', true);
		$data['menu'] = $this->load->view('plantilla/menu','', true);
		$this->load->view('envios/providencias',$data);


	}

	

	public function getComunastable()
	{

		$id = $this->input->get('id');
		$comunas=$this->asLogin->getComunas($id);

		$html='';

		foreach($comunas as $cat)
		{


			$idpr="";
			if(!empty($cat->idProvince)){
				$idpr=$cat->idProvince;

			}

			$idprnom="";
			if(!empty($cat->procince_name)){
				$idprnom=$cat->procince_name;

			}

			$html.='<tr>

			<th>'.$id.'</th> 


			<th>'.$idpr.'</th>
			<th>'.$idprnom.'</th>


			</tr>';
			




		}

		echo $html;


	}

	public function getComunas()
	{

		$id = $this->input->get('id');
		$comunas=$this->asLogin->getComunas($id);

		$html=' <option value="-1">Ninguno</option>';

		foreach($comunas as $cat)
		{

			$html.=' <option value="'.$cat->idProvince.'">'.$cat->procince_name.'</option>';


		}

		echo $html;


	}


	public function guardar()
	{

		$campania = $this->input->post('campania');
		$estatus = $this->input->post('estatus');

		$parametros = array(
			"campania" => $campania,
			"estatus" => $estatus);

		$this->as->insertar($parametros);

	}

	public function captura()
	{

		$data['topbar'] = $this->load->view('plantilla/topbar','', true);
		$data['menu'] = $this->load->view('plantilla/menu','', true);
		$this->load->view('envios/captura',$data);


	}

	public function editar()
	{
		$id= $this->input->get('id');	
		$data['info'] = 	$this->as->getId($id);
		$data['topbar'] = $this->load->view('plantilla/topbar','', true);
		$data['menu'] = $this->load->view('plantilla/menu','', true);
		$this->load->view('envios/editar',$data);


	}

	public function guardarEditar()
	{

		$campania = $this->input->post('campania');
		$id = $this->input->post('id');
		$estatus = $this->input->post('estatus');

		$parametros = array(
			"campania" => $campania,
			"estatus" => $estatus);

		$this->as->update($id,$parametros);

	}





	public function index()
	{


		$filtro= $this->input->get('filtro');		
		$offset = $this->input->get('per_page');
		$uri_segment = 0;
		if ($offset == "") {
			$offset = 0;
		}


		$data['registros'] = $this->as->getcampanias($offset, $this->limite,$filtro);
		$config['base_url'] = base_url() . 'envios/index?filtro='.$filtro;
		$config['total_rows'] = $this->as->getcampaniascout($filtro);
	   $config['per_page'] = $this->limite; //Número de registros mostrados por páginas
	   $config['num_links'] = 5; //Número de links mostrados en la paginación
	   $config['page_query_string'] = true;
	   $config['full_tag_open'] = '<ul class="pagination justify-content-center">';
	   $config['first_tag_open'] = '<li class="page-item">';
	   $config['first_link'] = 'Primera'; //primer link
	   $config['first_tag_close'] = '</li>';
	   $config['last_tag_open'] = '<li class="page-item">';
	   $config['last_link'] = 'Última'; //último link
	   $config['last_tag_close'] = '</li>';
	   $config["uri_segment"] = $uri_segment; //el segmento de la paginación
	   $config['next_tag_open'] = '<li class="page-item">';
	   $config['next_link'] = 'Siguiente'; //siguiente link
	   $config['next_tag_close'] = '</li>';
	   $config['prev_tag_open'] = '<li class="page-item">';
	   $config['prev_link'] = 'Anterior'; //anterior link
	   $config['prev_tag_close'] = '</li>';
	   $config['num_tag_open'] = '<li class="page-item">';
	   $config['num_tag_close'] = '</li>';
	   $config['cur_tag_open'] = '<li class="page-item active"><a href="#" class="page-link" >';
	   $config['cur_tag_close'] = '</a></li>';
	   $config['full_tag_close'] = '</ul>';
	   $config['attributes'] = array('class' => 'page-link');
	   $this->pagination->initialize($config); //inicializamos la paginación        
	   $data["pagination"] = $this->pagination->create_links();
	   $data['topbar'] = $this->load->view('plantilla/topbar','', true);
	   $data['menu'] = $this->load->view('plantilla/menu','', true);
	   $this->load->view('envios/consulta',$data);

	}

}

