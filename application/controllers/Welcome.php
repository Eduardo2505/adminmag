<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	function __construct() {

		parent::__construct();
		$this->load->helper('url');
		$this->load->library('session');
		//$this->load->model('servicios/admin_servicios','asLogin');
		$this->load->model('users_models','user');
		$sessionId = $this->session->userdata('sessionId');



		if (strlen($sessionId) != 0) {
			redirect('/admin', 'refresh');
		}

	}

	public function index()
	{


		$this->load->view('welcome_message');	
	}

	public function login()
	{
		$email = $this->input->post('email');
		$pass = $this->input->post('password');


		$valo=$this->user->login($email,$pass);
		if($valo){

			$datosSesion = array(
				'sessionId' => 1,
				'username' => $email
			);

			$this->session->set_userdata($datosSesion);
			echo 1;

		}else{

			echo -1;
		}

		

		/*
		$cliente = new SoapClient(URL_WDSL,array('trace' => 1));
		$parametros = array(
			"key" => KEY,
			"secretKey" => SECRET_KEY,
			"username" => $email,
			"password" => $pass
		);
		$respuesta = $cliente->customerLogin($parametros);
		if ($respuesta->customerLoginResult->customerLoginData->status == 'OK') 
		{
			$datosSesion = array(
				'sessionId' => $respuesta->customerLoginResult->customerLoginData->sessionID,
				'customerName' => $respuesta->customerLoginResult->customerLoginData->customerName,
				'customerID' => $respuesta->customerLoginResult->customerLoginData->customerID,
				'username' => $email,
				'response' => $respuesta
			);

			$this->session->set_userdata($datosSesion);
			echo 1;

		} else 
		{
			echo -1;
		}*/
	}

	
}
?>