<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cupones extends CI_Controller {
	private $limite = 10;

	function __construct() {

		parent::__construct();
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->library('pagination');
		$this->load->model('servicios/cupones_servicios','asCupones');
		$sessionId = $this->session->userdata('sessionId');

		if (strlen($sessionId) == 0) {
			redirect('', 'refresh');
		}

	}


	public function cuponesGenerados()
	{

		$this->load->helper('file');
		$id = $this->input->get('id');
		$resultado=$this->asCupones->getkey($id);

		$data ='idkey_cupones,cliente,registro,estado,referencia,campania'."\n";

		if (isset($resultado)) {
			foreach ($resultado->result() as $rowx) {
				
				
				
				$data.= $rowx->idkey_cupones.','.$rowx->cliente.','.$rowx->registro.','.$rowx->estado.','.$rowx->referencia.','.$rowx->campania.''."\n";
			}
		}


		
		if ( ! write_file( URL_UPLOAD.'Cupones.csv', $data))
		{
			echo 0;
		}
		else
		{
			echo 1;//redirect('uploads/Cupones.csv', 'refresh');
		}


	}

	public function key()
	{

		
		$filtro= $this->input->get('filtro');		
		$offset = $this->input->get('per_page');
		$uri_segment = 0;
		if ($offset == "") {
			$offset = 0;
		}

		
		$data['registros'] = $this->asCupones->getkeybus($offset, $this->limite,$filtro);
		$config['base_url'] = base_url() . 'cupones/key?filtro='.$filtro;
		$config['total_rows'] = $this->asCupones->getkeybuscount($filtro);
	   $config['per_page'] = $this->limite; //Número de registros mostrados por páginas
	   $config['num_links'] = 5; //Número de links mostrados en la paginación
	   $config['page_query_string'] = true;
	   $config['full_tag_open'] = '<ul class="pagination justify-content-center">';
	   $config['first_tag_open'] = '<li class="page-item">';
	   $config['first_link'] = 'Primera'; //primer link
	   $config['first_tag_close'] = '</li>';
	   $config['last_tag_open'] = '<li class="page-item">';
	   $config['last_link'] = 'Última'; //último link
	   $config['last_tag_close'] = '</li>';
	   $config["uri_segment"] = $uri_segment; //el segmento de la paginación
	   $config['next_tag_open'] = '<li class="page-item">';
	   $config['next_link'] = 'Siguiente'; //siguiente link
	   $config['next_tag_close'] = '</li>';
	   $config['prev_tag_open'] = '<li class="page-item">';
	   $config['prev_link'] = 'Anterior'; //anterior link
	   $config['prev_tag_close'] = '</li>';
	   $config['num_tag_open'] = '<li class="page-item">';
	   $config['num_tag_close'] = '</li>';
	   $config['cur_tag_open'] = '<li class="page-item active"><a href="#" class="page-link" >';
	   $config['cur_tag_close'] = '</a></li>';
	   $config['full_tag_close'] = '</ul>';
	   $config['attributes'] = array('class' => 'page-link');
	   $this->pagination->initialize($config); //inicializamos la paginación        
	   $data["pagination"] = $this->pagination->create_links();
	   $data['topbar'] = $this->load->view('plantilla/topbar','', true);
	   $data['menu'] = $this->load->view('plantilla/menu','', true);
	   $this->load->view('cupones/key',$data);

	}

	public function index()
	{

		
		$filtro= $this->input->get('filtro');		
		$offset = $this->input->get('per_page');
		$uri_segment = 0;
		if ($offset == "") {
			$offset = 0;
		}

		
		$data['registros'] = $this->asCupones->getCampanias($offset, $this->limite,$filtro);
		$config['base_url'] = base_url() . 'cupones/index?filtro='.$filtro;
		$config['total_rows'] = $this->asCupones->getCampaniascount($filtro);
	   $config['per_page'] = $this->limite; //Número de registros mostrados por páginas
	   $config['num_links'] = 5; //Número de links mostrados en la paginación
	   $config['page_query_string'] = true;
	   $config['full_tag_open'] = '<ul class="pagination justify-content-center">';
	   $config['first_tag_open'] = '<li class="page-item">';
	   $config['first_link'] = 'Primera'; //primer link
	   $config['first_tag_close'] = '</li>';
	   $config['last_tag_open'] = '<li class="page-item">';
	   $config['last_link'] = 'Última'; //último link
	   $config['last_tag_close'] = '</li>';
	   $config["uri_segment"] = $uri_segment; //el segmento de la paginación
	   $config['next_tag_open'] = '<li class="page-item">';
	   $config['next_link'] = 'Siguiente'; //siguiente link
	   $config['next_tag_close'] = '</li>';
	   $config['prev_tag_open'] = '<li class="page-item">';
	   $config['prev_link'] = 'Anterior'; //anterior link
	   $config['prev_tag_close'] = '</li>';
	   $config['num_tag_open'] = '<li class="page-item">';
	   $config['num_tag_close'] = '</li>';
	   $config['cur_tag_open'] = '<li class="page-item active"><a href="#" class="page-link" >';
	   $config['cur_tag_close'] = '</a></li>';
	   $config['full_tag_close'] = '</ul>';
	   $config['attributes'] = array('class' => 'page-link');
	   $this->pagination->initialize($config); //inicializamos la paginación        
	   $data["pagination"] = $this->pagination->create_links();
	   $data['topbar'] = $this->load->view('plantilla/topbar','', true);
	   $data['menu'] = $this->load->view('plantilla/menu','', true);
	   $this->load->view('cupones/consulta',$data);

	}
	public function captura()
	{
		$data['resultado'] =$this->asCupones->getCategorias();
		$data['productos'] =$this->asCupones->getProductos(-1);
		$data['topbar'] = $this->load->view('plantilla/topbar','', true);
		$data['menu'] = $this->load->view('plantilla/menu','', true);
		$this->load->view('cupones/captura',$data);

	}

	public function editar()
	{

		$id = $this->input->get('id');
		$info=$this->asCupones->getId($id);

		$data['info'] = $info;

		$tipo = $info->tipo;

		$hm='';
		

		if($tipo==2){

			$resultado=$this->asCupones->getCategorias();

			
			$data['none'] ="none";
			$data['pnone'] ="none";
			$data['tnone'] ="show";

			if (isset($resultado)) {
				foreach ($resultado->result() as $rowx) {

					$ver=$this->asCupones->getProductosverificarcateg($id,$rowx->IdCategory);

					if($ver!=0){
						$hm.='<option value="'.$rowx->IdCategory.'"  selected >'.$rowx->NameCategory.'</option>';

					}else{
						$hm.='<option value="'.$rowx->IdCategory.'"  >'.$rowx->NameCategory.'</option>';
					}

					

				}
			}
		}else{

			$resultado=$this->asCupones->getProducts($id,$tipo);
			$data['none'] ="show";
			$data['pnone']="show";
			$data['tnone'] ="none";
			if (isset($resultado)) {
				foreach ($resultado->result() as $rowx) {

					$hm.='<option value="'.$rowx->idp.'"  selected >'.$rowx->idp.'</option>';
					
				}
			}

		}




		$data['productos'] =$hm;
		$data['tipo'] =$tipo;
		$data['resultado'] =$this->asCupones->getCategorias();
		$data['topbar'] = $this->load->view('plantilla/topbar','', true);
		$data['menu'] = $this->load->view('plantilla/menu','', true);
		$this->load->view('cupones/editar',$data);

	}
	public function cambioEstatus()
	{
		$datos = array(
			"estado" => $this->input->get('estado'));
		$this->asCupones->editar($this->input->get('id'),$datos);


	}

	public function update()
	{

		$referencia = $this->input->post('referencia');
		$id = $this->input->post('id');
		$campania = $this->input->post('campania');
		$inicio = $this->input->post('inicio');
		$fecha_final = $this->input->post('final');
		$tipo = $this->input->post('tipo');

		$cantidad = $this->input->post('cantidad');
		$prefijo = $this->input->post('prefijo');
		$cantidad_utilizadas = $this->input->post('cantidadutlizados');
		$infinito = $this->input->post('infinito');

		$des_valor_tipo = $this->input->post('selectValorDescuento');
		$valor_des_tipo = $this->input->post('valorDescuento');
		$des_envio_tipo = $this->input->post('selectValorEnvio');
		$valor_des_envio = $this->input->post('valorEnvio');

		$parametros = array(
			"referencia" => $referencia,
			"campania" => $referencia,
			"fecha_inicio" => $inicio,
			"fecha_final" => $fecha_final,
			"tipo" => $tipo,
			"cantidad" => $cantidad,
			"prefijo" => $prefijo,
			"cantidad_utilizadas" => $cantidad_utilizadas,
			"infinito" => $infinito,
			"des_valor_tipo" => $des_valor_tipo,
			"valor_des_tipo" => $valor_des_tipo,
			"des_envio_tipo" => $des_envio_tipo,
			"valor_des_envio" => $valor_des_envio
		);
		$productosOcategorias = $this->input->post('productosOcategorias');



		$this->asCupones->update($parametros,$id,$tipo,$productosOcategorias);


	}
	public function guardar()
	{

		$referencia = $this->input->post('referencia');
		$campania = $this->input->post('campania');
		$inicio = $this->input->post('inicio');
		$fecha_final = $this->input->post('final');
		$tipo = $this->input->post('tipo');

		$cantidad = $this->input->post('cantidad');
		$prefijo = $this->input->post('prefijo');
		$cantidad_utilizadas = $this->input->post('cantidadutlizados');
		$infinito = $this->input->post('infinito');

		$des_valor_tipo = $this->input->post('selectValorDescuento');
		$valor_des_tipo = $this->input->post('valorDescuento');
		$des_envio_tipo = $this->input->post('selectValorEnvio');
		$valor_des_envio = $this->input->post('valorEnvio');

		$parametros = array(
			"referencia" => $referencia,
			"campania" => $referencia,
			"fecha_inicio" => $inicio,
			"fecha_final" => $fecha_final,
			"tipo" => $tipo,
			"cantidad" => $cantidad,
			"prefijo" => $prefijo,
			"cantidad_utilizadas" => $cantidad_utilizadas,
			"infinito" => $infinito,
			"des_valor_tipo" => $des_valor_tipo,
			"valor_des_tipo" => $valor_des_tipo,
			"des_envio_tipo" => $des_envio_tipo,
			"valor_des_envio" => $valor_des_envio,
			"estado" => 1
		);
		$productosOcategorias = $this->input->post('productosOcategorias');

		$this->asCupones->guardar($tipo,$parametros,$productosOcategorias);


	}


	public function productosCateEditar()
	{
		$id = $this->input->get('IdCategory');
		$idver = $this->input->get('idver');

		$hm='';


		$resultado=$this->asCupones->getProductos($id);

		if (isset($resultado)) {
			foreach ($resultado->result() as $rowx) {
				$verfi=$this->asCupones->getProductosverificar($idver,$rowx->ProductReference);

				if($verfi==0){
					$hm.='<option value="'.$rowx->ProductReference.'">'.$rowx->ProductReference.'</option>';
				}
			}
		}


		echo $hm;
	}


	public function productosCate()
	{
		$id = $this->input->get('IdCategory');

		$hm='';

		$resultado=$this->asCupones->getProductos($id);

		if (isset($resultado)) {
			foreach ($resultado->result() as $rowx) {
				$hm.='<option value="'.$rowx->ProductReference.'">'.$rowx->ProductReference.'</option>';
			}
		}


		echo $hm;
	}

	public function selectTipo()
	{
		$tipo = $this->input->get('tipo');


		$hm='';
		if($tipo==2){
			$resultado=$this->asCupones->getCategorias();

			if (isset($resultado)) {
				foreach ($resultado->result() as $rowx) {
					$hm.='<option value="'.$rowx->IdCategory.'">'.$rowx->NameCategory.'</option>';
				}
			}
		}else{

			$resultado=$this->asCupones->getProductos(-1);

			if (isset($resultado)) {
				foreach ($resultado->result() as $rowx) {
					$hm.='<option value="'.$rowx->ProductReference.'">'.$rowx->ProductReference.'</option>';
				}
			}


		}



		echo $hm;
	}
}
