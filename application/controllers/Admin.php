<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
	private $limite = 10;
	function __construct() {

		parent::__construct();
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->library('pagination');
		$this->load->model('servicios/admin_servicios','asLogin');
		$this->load->model('facturacion_models','facturasm');
		$sessionId = $this->session->userdata('sessionId');

		if (strlen($sessionId) == 0) {
			redirect('', 'refresh');
		}

	}

	public function facturas()
	{

		
		$filtro= $this->input->get('filtro');		
		$offset = $this->input->get('per_page');
		$uri_segment = 0;
		if ($offset == "") {
			$offset = 0;
		}

		//echo $filtro;

		
		$data['registros'] = $this->facturasm->get($offset, $this->limite,$filtro);
		$config['base_url'] = base_url() . 'admin/facturas?filtro='.$filtro;
		$config['total_rows'] = $this->facturasm->count($filtro);
	   $config['per_page'] = $this->limite; //Número de registros mostrados por páginas
	   $config['num_links'] = 5; //Número de links mostrados en la paginación
	   $config['page_query_string'] = true;
	   $config['full_tag_open'] = '<ul class="pagination justify-content-center">';
	   $config['first_tag_open'] = '<li class="page-item">';
	   $config['first_link'] = 'Primera'; //primer link
	   $config['first_tag_close'] = '</li>';
	   $config['last_tag_open'] = '<li class="page-item">';
	   $config['last_link'] = 'Última'; //último link
	   $config['last_tag_close'] = '</li>';
	   $config["uri_segment"] = $uri_segment; //el segmento de la paginación
	   $config['next_tag_open'] = '<li class="page-item">';
	   $config['next_link'] = 'Siguiente'; //siguiente link
	   $config['next_tag_close'] = '</li>';
	   $config['prev_tag_open'] = '<li class="page-item">';
	   $config['prev_link'] = 'Anterior'; //anterior link
	   $config['prev_tag_close'] = '</li>';
	   $config['num_tag_open'] = '<li class="page-item">';
	   $config['num_tag_close'] = '</li>';
	   $config['cur_tag_open'] = '<li class="page-item active"><a href="#" class="page-link" >';
	   $config['cur_tag_close'] = '</a></li>';
	   $config['full_tag_close'] = '</ul>';
	   $config['attributes'] = array('class' => 'page-link');
	   $this->pagination->initialize($config); //inicializamos la paginación        
	   $data["pagination"] = $this->pagination->create_links();
	   $data['topbar'] = $this->load->view('plantilla/topbar','', true);
	   $data['menu'] = $this->load->view('plantilla/menu','', true);
	   $this->load->view('historial/consulta',$data);

	}


	public function index()
	{

		$data['topbar'] = $this->load->view('plantilla/topbar','', true);
		$data['menu'] = $this->load->view('plantilla/menu','', true);
		$data['crontab'] = $this->asLogin->verificarCron();
		$this->load->view('dashboard/dashboard',$data);
		
	}

	public function sincronizarProdutos()
	{

		$this->asLogin->activarCron();
		$this->exce();
		redirect('admin', 'refresh');

		
	}
	public function exce()
	{
		//  /usr/local/bin/php /home/impreyac/public_html/adminmag/index.php crontab/sincronizarProdutosCron >> /home/impreyac/public_html/adminmag/application/logs/crontab_impreya.log &
		// /usr/local/bin/php /home/impreyac/magifoto.com.co/adminmag/index.php crontab/sincronizarProdutosCron >> /home/impreyac/public_html/adminmag/application/logs/crontab_magifoto.log &
		$message=shell_exec("/usr/local/bin/php /home/impreyac/public_html/adminmag/index.php crontab/sincronizarProdutosCron >> /home/impreyac/public_html/adminmag/application/logs/crontab_impreya.log &");
		print_r($message);
	}

	public function getRegiones()
	{

		$regiones=$this->asLogin->getRegiones();

		foreach($regiones as $cat)
		{

			echo $cat->idState." ".$cat->state_name."<br>";
		}

		
	}
	public function getComunas()
	{

		$comunas=$this->asLogin->getComunas(153);

		foreach($comunas as $cat)
		{

			echo $cat->idProvince." ".$cat->procince_name."<br>";
		}

		
	}



	public function login()
	{
		echo "Admin";
	}
}
