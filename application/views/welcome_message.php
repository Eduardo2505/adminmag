<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo site_url('') ?>assets/images/favicon.png">
    <title>Apilink</title>
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo site_url('') ?>assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- page css -->
    <link href="<?php echo site_url('') ?>assets/css/pages/login-register-lock.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo site_url('') ?>assets/css/style.css" rel="stylesheet">
    
    <!-- You can change the theme colors from here -->
    <link href="<?php echo site_url('') ?>assets/css/colors/blue-dark.css" id="theme" rel="stylesheet">

</head>

<body class="card-no-border">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">Apilink</p>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <section id="wrapper">
        <div class="login-register" style="background-image:url(<?php echo site_url('') ?>assets/images/background/login-register.jpg);">
            <div class="login-box card">
                <div class="card-body">
                    <form class="form-horizontal form-material" id="loginform">
                        <h3 class="box-title m-b-20">Ingresar</h3>
                        <div class="form-group ">
                            <div class="col-xs-12">
                                <input class="form-control" name="email"  type="email" required="" placeholder="Correo Eléctronico"> </div>
                            </div>
                            <div class="form-group">
                                <div class="col-xs-12">
                                    <input class="form-control" name="password"  type="password" required="" placeholder="Constraseña"> </div>
                                </div>
                                <div class="form-control-feedback" id="emailuser"style="display: none" ><small><code>Email o password incorrectos, verifique la informacón</code></small></div>

               
                                    <div class="form-group text-center">
                                        <div class="col-xs-12 p-b-20">
                                            <button class="btn btn-block btn-lg btn-info btn-rounded" type="submit">Entrar</button>
                                        </div>
                                    </div>


                               </form>

                            </div>
                        </div>
                    </div>
                </section>

                <!-- ============================================================== -->
                <!-- End Wrapper -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- All Jquery -->
                <!-- ============================================================== -->
                <script src="<?php echo site_url('') ?>assets/plugins/jquery/jquery.min.js"></script>
                <!-- Bootstrap tether Core JavaScript -->
                <script src="<?php echo site_url('') ?>assets/plugins/bootstrap/js/popper.min.js"></script>
                <script src="<?php echo site_url('') ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
                <!--Custom JavaScript -->
                <script type="text/javascript">
                    $(function() {
                        $(".preloader").fadeOut();
                    });

                    $("#loginform").submit(function(e) {

                     $(".preloader").fadeIn();
                       var url = "<?php echo site_url('') ?>welcome/login";
                       $.ajax({
                         type: "POST",
                         url: url,
                         data: $("#loginform").serialize(), 
                         success: function(data)

                         {
                           $(".preloader").fadeOut();
                            if(data==-1){
                                $("#emailuser").css("display", "block");
                            }else{
                                $(location).attr('href','<?php echo site_url('') ?>admin');
                            }


                        }
                    });
                       e.preventDefault(); 
                   });
               </script>

           </body>

           </html>