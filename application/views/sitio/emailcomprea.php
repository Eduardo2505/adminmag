<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:v="urn:schemas-microsoft-com:vml">
<head>
  <!--[if gte mso 9]><xml><o:OfficeDocumentSettings><o:AllowPNG/><o:PixelsPerInch>96</o:PixelsPerInch></o:OfficeDocumentSettings></xml><![endif]-->
  <meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
  <meta content="width=device-width" name="viewport"/>
  <!--[if !mso]><!-->
  <meta content="IE=edge" http-equiv="X-UA-Compatible"/>
  <!--<![endif]-->
  <title></title>
  <!--[if !mso]><!-->
  <!--<![endif]-->
  <style type="text/css">
    body {
      margin: 0;
      padding: 0;
    }

    table,
    td,
    tr {
      vertical-align: top;
      border-collapse: collapse;
    }

    * {
      line-height: inherit;
    }

    a[x-apple-data-detectors=true] {
      color: inherit !important;
      text-decoration: none !important;
    }

    .ie-browser table {
      table-layout: fixed;
    }

    [owa] .img-container div,
    [owa] .img-container button {
      display: block !important;
    }

    [owa] .fullwidth button {
      width: 100% !important;
    }

    [owa] .block-grid .col {
      display: table-cell;
      float: none !important;
      vertical-align: top;
    }

    .ie-browser .block-grid,
    .ie-browser .num12,
    [owa] .num12,
    [owa] .block-grid {
      width: 600px !important;
    }

    .ie-browser .mixed-two-up .num4,
    [owa] .mixed-two-up .num4 {
      width: 200px !important;
    }

    .ie-browser .mixed-two-up .num8,
    [owa] .mixed-two-up .num8 {
      width: 400px !important;
    }

    .ie-browser .block-grid.two-up .col,
    [owa] .block-grid.two-up .col {
      width: 300px !important;
    }

    .ie-browser .block-grid.three-up .col,
    [owa] .block-grid.three-up .col {
      width: 300px !important;
    }

    .ie-browser .block-grid.four-up .col [owa] .block-grid.four-up .col {
      width: 150px !important;
    }

    .ie-browser .block-grid.five-up .col [owa] .block-grid.five-up .col {
      width: 120px !important;
    }

    .ie-browser .block-grid.six-up .col,
    [owa] .block-grid.six-up .col {
      width: 100px !important;
    }

    .ie-browser .block-grid.seven-up .col,
    [owa] .block-grid.seven-up .col {
      width: 85px !important;
    }

    .ie-browser .block-grid.eight-up .col,
    [owa] .block-grid.eight-up .col {
      width: 75px !important;
    }

    .ie-browser .block-grid.nine-up .col,
    [owa] .block-grid.nine-up .col {
      width: 66px !important;
    }

    .ie-browser .block-grid.ten-up .col,
    [owa] .block-grid.ten-up .col {
      width: 60px !important;
    }

    .ie-browser .block-grid.eleven-up .col,
    [owa] .block-grid.eleven-up .col {
      width: 54px !important;
    }

    .ie-browser .block-grid.twelve-up .col,
    [owa] .block-grid.twelve-up .col {
      width: 50px !important;
    }
  </style>
  <style id="media-query" type="text/css">
    @media only screen and (min-width: 620px) {
      .block-grid {
        width: 600px !important;
      }

      .block-grid .col {
        vertical-align: top;
      }

      .block-grid .col.num12 {
        width: 600px !important;
      }

      .block-grid.mixed-two-up .col.num3 {
        width: 150px !important;
      }

      .block-grid.mixed-two-up .col.num4 {
        width: 200px !important;
      }

      .block-grid.mixed-two-up .col.num8 {
        width: 400px !important;
      }

      .block-grid.mixed-two-up .col.num9 {
        width: 450px !important;
      }

      .block-grid.two-up .col {
        width: 300px !important;
      }

      .block-grid.three-up .col {
        width: 200px !important;
      }

      .block-grid.four-up .col {
        width: 150px !important;
      }

      .block-grid.five-up .col {
        width: 120px !important;
      }

      .block-grid.six-up .col {
        width: 100px !important;
      }

      .block-grid.seven-up .col {
        width: 85px !important;
      }

      .block-grid.eight-up .col {
        width: 75px !important;
      }

      .block-grid.nine-up .col {
        width: 66px !important;
      }

      .block-grid.ten-up .col {
        width: 60px !important;
      }

      .block-grid.eleven-up .col {
        width: 54px !important;
      }

      .block-grid.twelve-up .col {
        width: 50px !important;
      }
    }

    @media (max-width: 620px) {

      .block-grid,
      .col {
        min-width: 320px !important;
        max-width: 100% !important;
        display: block !important;
      }

      .block-grid {
        width: 100% !important;
      }

      .col {
        width: 100% !important;
      }

      .col>div {
        margin: 0 auto;
      }

      img.fullwidth,
      img.fullwidthOnMobile {
        max-width: 100% !important;
      }

      .no-stack .col {
        min-width: 0 !important;
        display: table-cell !important;
      }

      .no-stack.two-up .col {
        width: 50% !important;
      }

      .no-stack .col.num4 {
        width: 33% !important;
      }

      .no-stack .col.num8 {
        width: 66% !important;
      }

      .no-stack .col.num4 {
        width: 33% !important;
      }

      .no-stack .col.num3 {
        width: 25% !important;
      }

      .no-stack .col.num6 {
        width: 50% !important;
      }

      .no-stack .col.num9 {
        width: 75% !important;
      }

      .video-block {
        max-width: none !important;
      }

      .mobile_hide {
        min-height: 0px;
        max-height: 0px;
        max-width: 0px;
        display: none;
        overflow: hidden;
        font-size: 0px;
      }

      .desktop_hide {
        display: block !important;
        max-height: none !important;
      }
    }
  </style>
</head>
<body class="clean-body" style="margin: 0; padding: 0; -webkit-text-size-adjust: 100%; background-color: #EAE3DD;">
  <style id="media-query-bodytag" type="text/css">
    @media (max-width: 620px) {
      .block-grid {
        min-width: 320px!important;
        max-width: 100%!important;
        width: 100%!important;
        display: block!important;
      }
      .col {
        min-width: 320px!important;
        max-width: 100%!important;
        width: 100%!important;
        display: block!important;
      }
      .col > div {
        margin: 0 auto;
      }
      img.fullwidth {
        max-width: 100%!important;
        height: auto!important;
      }
      img.fullwidthOnMobile {
        max-width: 100%!important;
        height: auto!important;
      }
      .no-stack .col {
        min-width: 0!important;
        display: table-cell!important;
      }
      .no-stack.two-up .col {
        width: 50%!important;
      }
      .no-stack.mixed-two-up .col.num4 {
        width: 33%!important;
      }
      .no-stack.mixed-two-up .col.num8 {
        width: 66%!important;
      }
      .no-stack.three-up .col.num4 {
        width: 33%!important
      }
      .no-stack.four-up .col.num3 {
        width: 25%!important
      }
    }
  </style>
  <!--[if IE]><div class="ie-browser"><![endif]-->
    <table bgcolor="#EAE3DD" cellpadding="0" cellspacing="0" class="nl-container" role="presentation" style="table-layout: fixed; vertical-align: top; min-width: 320px; Margin: 0 auto; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; background-color: #EAE3DD; width: 100%;" valign="top" width="100%">
      <tbody>
        <tr style="vertical-align: top;" valign="top">
          <td style="word-break: break-word; vertical-align: top; border-collapse: collapse;" valign="top">
            <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td align="center" style="background-color:#EAE3DD"><![endif]-->
              <div style="background-color:#EAE3DD;">
                <div class="block-grid" style="Margin: 0 auto; min-width: 320px; max-width: 600px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: #ffffff;;">
                  <div style="border-collapse: collapse;display: table;width: 100%;background-color:#ffffff;">
                    <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0" style="background-color:#EAE3DD;"><tr><td align="center"><table cellpadding="0" cellspacing="0" border="0" style="width:600px"><tr class="layout-full-width" style="background-color:#ffffff"><![endif]-->
                      <!--[if (mso)|(IE)]><td align="center" width="600" style="background-color:#ffffff;width:600px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 0px; padding-left: 0px; padding-top:0px; padding-bottom:0px;"><![endif]-->
                        <div class="col num12" style="min-width: 320px; max-width: 600px; display: table-cell; vertical-align: top;;">
                          <div style="width:100% !important;">
                            <!--[if (!mso)&(!IE)]><!-->
                            <div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:0px; padding-bottom:0px; padding-right: 0px; padding-left: 0px;">
                              <!--<![endif]-->
                              <div align="center" class="img-container center autowidth" style="padding-right: 15px;padding-left: 15px;">
                                <!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr style="line-height:0px"><td style="padding-right: 15px;padding-left: 15px;" align="center"><![endif]-->
                                  <div style="font-size:1px;line-height:15px"> </div><img align="center" alt="Image" border="0" class="center autowidth" src="<?php echo base_url(); ?>assets/images/logolow-2.png" style="outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; clear: both; border: 0; height: auto; float: none; width: 100%; max-width: 100px; display: block;" title="Image" width="100"/>
                                  <div style="font-size:1px;line-height:15px"> </div>
                                  <!--[if mso]></td></tr></table><![endif]-->
                                </div>

                              </div>
                              <!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 20px; padding-left: 20px; padding-top: 20px; padding-bottom: 20px; font-family: Arial, sans-serif"><![endif]-->
                                <div style="color:#892C63;font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif;line-height:120%;padding-top:20px;padding-right:20px;padding-bottom:20px;padding-left:20px;">
                                  <div style="font-size: 12px; line-height: 14px; color: #892C63; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif;">
                                    <div style="line-height: 14px; font-size: 12px; text-align: center;"><span style="font-size: 24px; line-height: 28px; font-family: 'lucida sans unicode', 'lucida grande', sans-serif;"><span style="line-height: 28px; font-size: 24px;">Pedido # <?php echo $info->referenceCode ?></span></span></div>
                                  </div>
                                </div>
                                <!--[if mso]></td></tr></table><![endif]-->
                                <!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 20px; padding-left: 20px; padding-top: 0px; padding-bottom: 0px; font-family: Arial, sans-serif"><![endif]-->
                                  <div style="color:#555555;font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif;line-height:180%;padding-top:0px;padding-right:20px;padding-bottom:0px;padding-left:20px;">
                                    <div style="font-size: 12px; line-height: 21px; color: #555555; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif;">
                                      <table align="center" border="0" cellpadding="0" cellspacing="0" class="divider_content" role="presentation" style="table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; border-top: 0px solid transparent;" valign="top" width="100%">

                                       <tr>

                                        <td >Nombre</td>
                                        <td ><?php echo $info->nombre ?></td>
                                      </tr>

                                      
                                      <?php  if($info->envioDomicilio==null){?>
                                        <tr>
                                          <td >Sucursal</td>
                                          <td ><?php echo $info->sucursal ?></td>
                                        </tr>
                                      <?php }else{?>
                                      
                                      <tr>
                                        <td >Calle</td>
                                        <td ><?php echo $info->calle ?>, <?php echo $info->shippingCountry; ?> ,<?php echo $info->shippingCity; ?></td>
                                      </tr>
                                     <?php }?>

                                      <tr>

                                        <td >Télefono</td>
                                        <td ><?php echo $info->telefono ?></td>
                                      </tr>
                                      <tr>
                                        <td >Referencia</td>
                                        <td ><?php echo $info->referenceCode ?></td>
                                      </tr>
                                      <tr>
                                        <td >Estatus Pedido</td>
                                        <td ><?php echo $info->transactionState ?></td>
                                      </th> 
                                    </table>


                                    <table align="center" border="0" cellpadding="0" cellspacing="0" class="divider_content" role="presentation" style="table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; border-top: 0px solid transparent;" valign="top" width="100%">

                                     <tr >
                                       <td ></td>
                                       <td >Referencia</td>
                                       <td >Proyecto</td>
                                       <td >Cantidad</td>
                                       <td >Precio</td>
                                     </tr>
                                     <tbody>



                                      <?php
                                      $um=0;
                                      foreach($registros->result() as $producto)
                                      {


                                        $costo_total_uni=$producto->costo_total_uni;
                                        $key_cupon=$producto->key_cupon;
                                        $disable='';
                                        $disablecantidad='';

                                        $disable='disabled="disabled"';     
                                        $descuento_valor=$producto->descuento_valor;
                                        $descuento_porcentaje=$producto->descuento_porcentaje;


                                        $costo_total_uni=$costo_total_uni-$descuento_valor;
                                        $disablecantidad='disabled="disabled"';


                                        if($descuento_porcentaje!=0){

                          //$costo_auz=$costo_total_uni* $descuento_valor;
                                          $costo_total_uni=$costo_total_uni* $descuento_porcentaje;


                                        }
                                        $sub=$costo_total_uni*$producto->cantidad;
                                        $um+= $sub;
                                        ?>


                                        <tr>
                                          <td > <img style="outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; clear: both; border: 0; height: auto; float: none; width: 100%; max-width: 100px; display: block;" title="Image" class="center autowidth" src="<?php echo $producto->projectThumbnail; ?>" width="100" height="100" style="border:solid; border-color:gray"/></td>
                                          <td ><?php echo $producto->projectCategory; ?> <?php echo $producto->referencia; ?></td>
                                          <td > <?php echo $producto->nombre_proyecto; ?></td>
                                          <td ><?php echo $producto->cantidad; ?></td>
                                          <td >$ <?php echo number_format( $sub, 0, '', '.'); ?></td>
                                        </tr>






                                        <?php
                                      }

                                      ?>
                                    </tbody>

                                    <th >

                                      <td ></td>
                                      <td ></td>
                                      <td >Total :</td>
                                      <td >$ <?php echo number_format( $um, 0, '', '.'); ?></td>
                                    </th>

                                    
                                  </table>

<br><br><br><br><br>
                                </div>
                              </div>
                              <!--[if mso]></td></tr></table><![endif]-->



                              <!--[if (!mso)&(!IE)]><!-->
                            </div>
                            <!--<![endif]-->
                          </div>
                        </div>
                        <!--[if (mso)|(IE)]></td></tr></table><![endif]-->
                        <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
                      </div>
                    </div>
                  </div>
                  <div style="background-color:transparent;">
                    <div class="block-grid" style="Margin: 0 auto; min-width: 320px; max-width: 600px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: transparent;;">
                      <div style="border-collapse: collapse;display: table;width: 100%;background-color:transparent;">
                        <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0" style="background-color:transparent;"><tr><td align="center"><table cellpadding="0" cellspacing="0" border="0" style="width:600px"><tr class="layout-full-width" style="background-color:transparent"><![endif]-->
                          <!--[if (mso)|(IE)]><td align="center" width="600" style="background-color:transparent;width:600px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 0px; padding-left: 0px; padding-top:5px; padding-bottom:5px;"><![endif]-->
                            <div class="col num12" style="min-width: 320px; max-width: 600px; display: table-cell; vertical-align: top;;">
                              <div style="width:100% !important;">
                                <!--[if (!mso)&(!IE)]><!-->
                                <div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;">
                                  <!--<![endif]-->
                                  <table border="0" cellpadding="0" cellspacing="0" class="divider" role="presentation" style="table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; min-width: 100%; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;" valign="top" width="100%">
                                    <tbody>
                                      <tr style="vertical-align: top;" valign="top">
                                        <td class="divider_inner" style="word-break: break-word; vertical-align: top; min-width: 100%; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-top: 5px; padding-right: 5px; padding-bottom: 5px; padding-left: 5px; border-collapse: collapse;" valign="top">
                                          <table align="center" border="0" cellpadding="0" cellspacing="0" class="divider_content" role="presentation" style="table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; border-top: 0px solid transparent;" valign="top" width="100%">
                                            <tbody>
                                              <tr style="vertical-align: top;" valign="top">
                                                <td style="word-break: break-word; vertical-align: top; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; border-collapse: collapse;" valign="top"><span></span></td>
                                              </tr>
                                            </tbody>
                                          </table>
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>
                                  <!--[if (!mso)&(!IE)]><!-->
                                </div>
                                <!--<![endif]-->
                              </div>
                            </div>
                            <!--[if (mso)|(IE)]></td></tr></table><![endif]-->
                            <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
                          </div>
                        </div>
                      </div>
                      <div style="background-color:#EAE3DD;">
                        <div class="block-grid" style="Margin: 0 auto; min-width: 320px; max-width: 600px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: #892C63;;">
                          <div style="border-collapse: collapse;display: table;width: 100%;background-color:#892C63;">
                            <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0" style="background-color:#EAE3DD;"><tr><td align="center"><table cellpadding="0" cellspacing="0" border="0" style="width:600px"><tr class="layout-full-width" style="background-color:#892C63"><![endif]-->
                              <!--[if (mso)|(IE)]><td align="center" width="600" style="background-color:#892C63;width:600px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 0px; padding-left: 0px; padding-top:0px; padding-bottom:0px;"><![endif]-->
                                <div class="col num12" style="min-width: 320px; max-width: 600px; display: table-cell; vertical-align: top;;">
                                  <div style="width:100% !important;">
                                    <!--[if (!mso)&(!IE)]><!-->
                                    <div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:0px; padding-bottom:0px; padding-right: 0px; padding-left: 0px;">
                                      <!--<![endif]-->
                                      <!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 0px; padding-left: 0px; padding-top: 20px; padding-bottom: 20px; font-family: Arial, sans-serif"><![endif]-->

                                        <!--[if mso]></td></tr></table><![endif]-->
                                        <!--[if (!mso)&(!IE)]><!-->
                                      </div>
                                      <!--<![endif]-->
                                    </div>
                                  </div>
                                  <!--[if (mso)|(IE)]></td></tr></table><![endif]-->
                                  <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
                                </div>
                              </div>
                            </div>

                            <div style="background-color:#EAE3DD;">
                              <div class="block-grid" style="Margin: 0 auto; min-width: 320px; max-width: 600px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: #EAE3DD;;">
                                <div style="border-collapse: collapse;display: table;width: 100%;background-color:#EAE3DD;">
                                  <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0" style="background-color:#EAE3DD;"><tr><td align="center"><table cellpadding="0" cellspacing="0" border="0" style="width:600px"><tr class="layout-full-width" style="background-color:#EAE3DD"><![endif]-->
                                    <!--[if (mso)|(IE)]><td align="center" width="600" style="background-color:#EAE3DD;width:600px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 0px; padding-left: 0px; padding-top:0px; padding-bottom:0px;"><![endif]-->
                                      <div class="col num12" style="min-width: 320px; max-width: 600px; display: table-cell; vertical-align: top;;">
                                        <div style="width:100% !important;">
                                          <!--[if (!mso)&(!IE)]><!-->
                                          <div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:0px; padding-bottom:0px; padding-right: 0px; padding-left: 0px;">
                                            <!--<![endif]-->
                                            <table cellpadding="0" cellspacing="0" class="social_icons" role="presentation" style="table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;" valign="top" width="100%">
                                              <tbody>
                                                <tr style="vertical-align: top;" valign="top">
                                                  <td style="word-break: break-word; vertical-align: top; padding-top: 30px; padding-right: 0px; padding-bottom: 20px; padding-left: 0px; border-collapse: collapse;" valign="top">
                                                    <table activate="activate" align="center" alignment="alignment" cellpadding="0" cellspacing="0" class="social_table" role="presentation" style="table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: undefined; mso-table-tspace: 0; mso-table-rspace: 0; mso-table-bspace: 0; mso-table-lspace: 0;" to="to" valign="top">
                                                      <tbody>
                                                        <tr align="center" style="vertical-align: top; display: inline-block; text-align: center;" valign="top">
                                                          <td style="word-break: break-word; vertical-align: top; padding-bottom: 5px; padding-right: 5px; padding-left: 5px; border-collapse: collapse;" valign="top"><a href="https://www.facebook.com/" target="_blank"><img alt="Facebook" height="32" src="<?php echo base_url(); ?>assets/images/facebook.png" style="outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; clear: both; height: auto; float: none; border: none; display: block;" title="Facebook" width="32"/></a></td>
                                                          <td style="word-break: break-word; vertical-align: top; padding-bottom: 5px; padding-right: 5px; padding-left: 5px; border-collapse: collapse;" valign="top"><a href="http://twitter.com/" target="_blank"><img alt="Twitter" height="32" src="<?php echo base_url(); ?>assets/images/twitter.png" style="outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; clear: both; height: auto; float: none; border: none; display: block;" title="Twitter" width="32"/></a></td>
                                                         
                                                        </tr>
                                                      </tbody>
                                                    </table>
                                                  </td>
                                                </tr>
                                              </tbody>
                                            </table>
                                            <!--[if (!mso)&(!IE)]><!-->
                                          </div>
                                          <!--<![endif]-->
                                        </div>
                                      </div>
                                      <!--[if (mso)|(IE)]></td></tr></table><![endif]-->
                                      <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
                                    </div>
                                  </div>
                                </div>
                                <!--[if (mso)|(IE)]></td></tr></table><![endif]-->
                              </td>
                            </tr>
                          </tbody>
                        </table>
                        <!--[if (IE)]></div><![endif]-->
                      </body>
                      </html>