<!DOCTYPE html>
<html lang="en">

<head>
    <?php $this->load->view('plantilla/head'); ?>

</head>

<body class="fix-header card-no-border fix-sidebar">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">Apilink Systems</p>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar">
           <?php echo $topbar?>
       </header>
       <!-- ============================================================== -->
       <!-- End Topbar header -->
       <!-- ============================================================== -->
       <!-- ============================================================== -->
       <!-- Left Sidebar - style you can find in sidebar.scss  -->
       <!-- ============================================================== -->
       <aside class="left-sidebar">
        <?php echo $menu?>
    </aside>
    <!-- ============================================================== -->
    <!-- End Left Sidebar - style you can find in sidebar.scss  -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Page wrapper  -->
    <!-- ============================================================== -->
    <div class="page-wrapper">
        <!-- ============================================================== -->
        <!-- Horizontal navbar and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row hdr-nav-bar">
            <div class="col-md-12">
                <nav class="navbar navbar-expand-lg navbar-light">
                    <a class="navbar-brand hidden-lg-up">Menú</a>
                    <a class="navbar-toggler">
                        <span class="ti-menu" data-toggle="collapse" data-target="#navbarText"></span>
                    </a>
                    <div class="collapse navbar-collapse" id="navbarText">
                        <ul class="navbar-nav mr-auto">
                            <!-- Nav item -->
                            <li class="nav-item">
                                <a class="nav-link" href="<?php echo site_url('') ?>">Inicio</a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" href="javascript:void(0)">xxxx</a>
                            </li>

                            <li class="nav-item active">
                                <a class="nav-link" href="javascript:void(0)">xxxxx</a>
                            </li>

                        </ul>
                        
                    </div>
                </nav>
            </div>
            <div>

            </div>

        </div>
        <!-- ============================================================== -->
        <!-- End Horizontal navbar and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Container fluid  -->
        <!-- ============================================================== -->
        <div class="container-fluid">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor">xxxxxx
                    </h3>
                </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo site_url('') ?>">Inicio</a></li>
                        <li class="breadcrumb-item">xxxxx</li>
                        <li class="breadcrumb-item active">xxxx</li>
                    </ol>
                </div>
                <div class="">
                    <!-- <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button> -->
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Start Page Content -->
            <!-- ============================================================== -->

            <div class="row">

                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                         <h4 class="card-title">Busqueda Avanzada</h3>
                             
                            
                            <h4 class="card-title">xxxx</h4>

                            <div class="table-responsive">
                                
                            </div>










                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End PAge Content -->
            <!-- ============================================================== -->

        </div>
        <!-- ============================================================== -->
        <!-- End Container fluid  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- footer -->
        <!-- ============================================================== -->
        <footer class="footer"> <?php $this->load->view('plantilla/footer'); ?></footer>
        <!-- ============================================================== -->
        <!-- End footer -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Page wrapper  -->
    <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Wrapper -->
<!-- ============================================================== -->


<?php $this->load->view('plantilla/javascript'); ?>

</body>

</html>