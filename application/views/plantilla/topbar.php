
<nav class="navbar top-navbar navbar-expand-md navbar-light">
    <!-- ============================================================== -->
    <!-- Logo -->
    <!-- ============================================================== -->
    <div class="navbar-header">
        <a class="navbar-brand" href="<?php echo site_url(''); ?>">
            <!-- Logo icon --><b>
                <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                <!-- Dark Logo icon -->
                <img src="<?php echo site_url(''); ?>assets/images/logo-icon.png" alt="homepage" class="dark-logo" />
                <!-- Light Logo icon -->
                <img src="<?php echo site_url(''); ?>assets/images/logo-light-icon.png" alt="homepage" class="light-logo" />
            </b>
            <!--End Logo icon -->
            <!-- Logo text --><span>
               <!-- dark Logo text -->
               <img src="<?php echo site_url(''); ?>assets/images/logo-text.png" alt="homepage" class="dark-logo" />
               <!-- Light Logo text -->    
               <img src="<?php echo site_url(''); ?>assets/images/logo-light-text.png" class="light-logo" alt="homepage" /></span> </a>
           </div>
           <!-- ============================================================== -->
           <!-- End Logo -->
           <!-- ============================================================== -->
           <div class="navbar-collapse">
            <!-- ============================================================== -->
            <!-- toggle and nav items -->
            <!-- ============================================================== -->
            <ul class="navbar-nav mr-auto">
                <!-- This is  -->
                <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up waves-effect waves-dark" href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
                <li class="nav-item hidden-sm-down"></li>
            </ul>
            <!-- ============================================================== -->
            <!-- User profile and search -->
            <!-- ============================================================== -->
            <ul class="navbar-nav my-lg-0">

                                                            <!-- ============================================================== -->
                                                            <!-- Profile -->
                                                            <!-- ============================================================== -->
                                                            <li class="nav-item dropdown">
                                                                <a class="nav-link dropdown-toggle waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                    <i class="icon-user"></i>
                                                                <!-- <img src="<?php echo site_url(''); ?>assets/images/logo-light-icon.png" alt="user" class="profile-pic" /> -->
                                                                </a>
                                                                <div class="dropdown-menu dropdown-menu-right animated flipInY">
                                                                    <ul class="dropdown-user">
                                                                        <li>
                                                                            <div class="dw-user-box">
                                                                                <!-- <div class="u-img"><img src="<?php echo site_url(''); ?>assets/images/logo-light-icon.png" alt="user"></div> -->
                                                                                <div class="u-text">
                                                                                    <h4>  Hola! </h4>
                                                                                      <?php echo $this->session->userdata('username'); ?>
                                                                                    
                                                                                    <!-- <a href="<?php echo site_url(''); ?>perfil" class="btn btn-rounded btn-danger btn-sm">Perfil</a> -->
                                                                                </div>
                                                                                </div>
                                                                            </li>
                                                                           
                                                                            <li><a href="<?php echo site_url(''); ?>salir/close"><i class="fa fa-power-off"></i> Salir</a></li>
                                                                        </ul>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </nav>
