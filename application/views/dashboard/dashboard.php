<!DOCTYPE html>
<html lang="en">

<head>
    <link rel="stylesheet" href="<?php echo site_url('') ?>assets/plugins/venobox/venobox.css" type="text/css" media="screen" />
    <?php $this->load->view('plantilla/head'); ?>

</head>

<body class="fix-header card-no-border fix-sidebar">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">Apilink Systems</p>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar">
            <?php echo $topbar ?>
        </header>
        <aside class="left-sidebar">
            <?php echo $menu ?>
        </aside>

        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->

            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h3 class="text-themecolor">Sincornizar</h3>
                    </div>
                    <div class="col-md-7 align-self-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Inicio</a></li>
                            <li class="breadcrumb-item">Compras</li>
                        </ol>
                    </div>
                    <!--<div class="">
                <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
            </div> -->
                </div>

                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->

                <div class="row">

                    <div class="col-12">
                        <div class="card">
                            <div class="card-body" style="text-align: center;">

                                <?php if ($crontab == 0) { ?>
                                    <a onClick="load()" href="<?php echo site_url('') ?>admin/sincronizarProdutos" class="btn waves-effect waves-light btn-lg btn-primary">Sincronizar</a>

                                <?php } else { ?>
                                   <h4> ACTUALIZANDO PRODUCTOS PUEDE TARDAR ALGUNOS MINUTOS.</h4>
                                <?php } ?>
                                

                                <nav>

                                </nav>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->

                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer"> <?php $this->load->view('plantilla/footer'); ?></footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>


        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->

    <script src="<?php echo site_url('') ?>assets/plugins/jquery/jquery.min.js"></script>
    <script src="<?php echo site_url('') ?>assets/plugins/venobox/venobox.min.js"></script>
    <script src="https://unpkg.com/sweetalert2@7.8.2/dist/sweetalert2.all.js"></script>



    <?php $this->load->view('plantilla/javascript'); ?>

    <script type="text/javascript">
        function load() {

            $(".preloader").fadeIn();

        }
    </script>

</body>

</html>