<!DOCTYPE html>
<html lang="en">

<head>

  <?php $this->load->view('plantilla/head'); ?>

</head>

<body class="fix-header card-no-border fix-sidebar">
  <!-- ============================================================== -->
  <!-- Preloader - style you can find in spinners.css -->
  <!-- ============================================================== -->
  <div class="preloader">
    <div class="loader">
      <div class="loader__figure"></div>
      <p class="loader__label">Apilink Systems</p>
    </div>
  </div>
  <!-- ============================================================== -->
  <!-- Main wrapper - style you can find in pages.scss -->
  <!-- ============================================================== -->
  <div id="main-wrapper">
    <!-- ============================================================== -->
    <!-- Topbar header - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <header class="topbar">
     <?php echo $topbar?>
   </header>
   <aside class="left-sidebar">
    <?php echo $menu?>
  </aside>

  <!-- ============================================================== -->
  <!-- ============================================================== -->
  <!-- Page wrapper  -->
  <!-- ============================================================== -->
  <div class="page-wrapper">
    <!-- ============================================================== -->

    <!-- ============================================================== -->
    <div class="container-fluid">
      <!-- ============================================================== -->
      <!-- Bread crumb and right sidebar toggle -->
      <!-- ============================================================== -->
      <div class="row page-titles">
        <div class="col-md-5 align-self-center">
          <h3 class="text-themecolor">Providencias</h3>
        </div>
        <div class="col-md-7 align-self-center">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo site_url('') ?>">Inicio</a></li>
            <li class="breadcrumb-item">Consulta</li>
          </ol>
        </div>
            <!--<div class="">
                <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
              </div> -->
            </div>

            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Start Page Content -->
            <!-- ============================================================== -->
            <div class="row">
              <div class="col-md-12">


                <div class="card">
                  <div class="card-body">

                    <div id="education_fields"></div>
                    <div class="row">

                      <div class="col-sm-3 nopadding">
                        <div class="form-group">

                         <select id="regiones" name="regiones" class="form-control" required="" >
                          <option value="">Seleccione</option>

                          <?php
                          foreach($regiones as $cat)
                          {

                           ?>
                           <option value="<?php echo $cat->idState?>"> <?php echo $cat->state_name?>  </option>                        


                           <?php
                         }

                         ?>

                       </select>


                     </div>
                   </div>
                  


                </div>
              </div>
            </div>

          </div>
        </div>


       
        <div class="row">
        

            <div class="col-lg-12">
              <div class="card">
                <div class="card-body">


                  <div class="table-responsive">

                   <table id="tableCompras" class="table">
                    <thead>
                     <thead>
                      <tr>
                         
                         
                            <th>idRegion</th>

                            <th>idComuna</th>
                          
                            <th>Comuna</th> 

                          </tr>
                        </thead>
                        <tbody id="comunasr">
                          
                        </tbody>
                       

                    </table>


                  </div>

                </div>
                

              </div>
            </div>

          </form>

        </div>






    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Right sidebar -->
    <!-- ============================================================== -->
    <!-- .right-sidebar -->

    <!-- ============================================================== -->
    <!-- End Right sidebar -->
    <!-- ============================================================== -->
  </div>
  <!-- ============================================================== -->
  <!-- End Container fluid  -->
  <!-- ============================================================== -->
  <!-- End Container fluid  -->
  <!-- ============================================================== -->
  <!-- ============================================================== -->
  <!-- footer -->
  <!-- ============================================================== -->
  <footer class="footer"> <?php $this->load->view('plantilla/footer'); ?></footer>
  <!-- ============================================================== -->
  <!-- End footer -->
  <!-- ============================================================== -->
</div>


<!-- ============================================================== -->
<!-- End Page wrapper  -->
<!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Wrapper -->
<!-- ============================================================== -->






<?php $this->load->view('plantilla/javascript'); ?>

<script>
  jQuery(document).ready(function() {



    $("#regiones").change(function(e) {


     $(".preloader").fadeIn();
     var url = "<?php echo site_url('') ?>envios/getComunastable";
     var id=$("#regiones").val();
     var dataenvio="id="+id;

     $.ajax({
       type: "GET",
       url: url,
       data: dataenvio, 
       success: function(data)

       {


         $('#comunasr').html(data);
         $(".preloader").fadeOut();




       }
     });
     e.preventDefault(); 
   });

    

  });
</script>


</body>

</html>