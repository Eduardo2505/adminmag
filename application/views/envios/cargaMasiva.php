<!DOCTYPE html>
<html lang="en">

<head>

  <?php $this->load->view('plantilla/head'); ?>

</head>

<body class="fix-header card-no-border fix-sidebar">
  <!-- ============================================================== -->
  <!-- Preloader - style you can find in spinners.css -->
  <!-- ============================================================== -->
  <div class="preloader">
    <div class="loader">
      <div class="loader__figure"></div>
      <p class="loader__label">Apilink Systems</p>
    </div>
  </div>
  <!-- ============================================================== -->
  <!-- Main wrapper - style you can find in pages.scss -->
  <!-- ============================================================== -->
  <div id="main-wrapper">
    <!-- ============================================================== -->
    <!-- Topbar header - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <header class="topbar">
     <?php echo $topbar?>
   </header>
   <aside class="left-sidebar">
    <?php echo $menu?>
  </aside>

  <!-- ============================================================== -->
  <!-- ============================================================== -->
  <!-- Page wrapper  -->
  <!-- ============================================================== -->
  <div class="page-wrapper">
    <!-- ============================================================== -->

    <!-- ============================================================== -->
    <div class="container-fluid">
      <!-- ============================================================== -->
      <!-- Bread crumb and right sidebar toggle -->
      <!-- ============================================================== -->
      <div class="row page-titles">
        <div class="col-md-5 align-self-center">
          <h3 class="text-themecolor">Providencias</h3>
        </div>
        <div class="col-md-7 align-self-center">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo site_url('') ?>">Inicio</a></li>
            <li class="breadcrumb-item">Consulta</li>
          </ol>
        </div>
            <!--<div class="">
                <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
              </div> -->
            </div>

            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Start Page Content -->
            <!-- ============================================================== -->
            <div class="row">
              <div class="col-md-12">

                <form enctype="multipart/form-data" method="post" action="<?php echo site_url('') ?>envios/validarArchivo">
                 <input type="hidden" name="id" value="<?php echo $idcampania;?>">
                 <div class="card">
                  <div class="card-body">

                    <div id="education_fields"></div>
                    <div class="row">

                      <div class="col-sm-3 nopadding">
                        <div class="form-group">

                          CSV : <input type="file" class="form-control" name="file" id="file">




                        </select>


                      </div>
                    </div>




                    <div class="col-sm-6 nopadding">
                      <div class="form-group">
                       <input type="submit" class="btn btn-info" value="Enviar" name="enviar">

                       <a href="<?php echo site_url('') ?>envios/providencias?id=<?php echo $idcampania; ?>"  class="btn btn-success">Regresar</a>
                       <a href="<?php echo site_url('') ?>uploads/CargaMasiva.csv"  class="btn btn-success">CSV</a>


                     </div>
                   </div>

                 </div>
               </div>
             </div>
           </form>

         </div>
       </div>


       <?php
       if (isset($prodcateaux)) {



         ?>
         <div class="row">
          <form action="<?php echo site_url('') ?>envios/guardarMasiva" method="POST">
            <input type="hidden" name="id" value="<?php echo $idcampania;?>">
            

            <div class="col-lg-12">
              <div class="card">
                <div class="card-body">


                  <div class="table-responsive">

                   <table id="tableCompras" class="table">
                    <thead>
                     <thead>
                      <tr>

                        <th>id_Region</th>
                        <th>nombre_region</th>
                        <th>id_comuna</th>
                        <th>nombre_comuna</th>
                        <th>precio</th> 
                        <th>precio_extra</th> 

                      </tr>
                    </thead>
                    <?php
               
                    foreach($prodcateaux as $item) {

                     
                        $erro=$item['error'];

                        ?>


                        <tr <?php echo ($erro==1) ? "style='background-color: coral' ":""; ?> >


                          <td><?php echo $item['id_Region']; ?></td>
                          <td><?php echo $item['nombre_region']; ?></td>
                          <td><?php echo $item['id_comuna']; ?></td>
                          <td><?php echo $item['nombre_comuna']; ?></td>
                          <td><?php echo $item['precio']; ?></td>
                          <td><?php echo $item['precio_extra']; ?></td>



                        </tr>



                        <?php
                     
                     
                    }

                    ?>



                  </table>


                </div>

              </div>
              <input type="submit" value="Guardar" class="btn btn-info btn-rounded m-t-10 float-right" >

            </div>
          </div>

        </form>

      </div>
      <?php

    }
    ?>   





    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Right sidebar -->
    <!-- ============================================================== -->
    <!-- .right-sidebar -->

    <!-- ============================================================== -->
    <!-- End Right sidebar -->
    <!-- ============================================================== -->
  </div>
  <!-- ============================================================== -->
  <!-- End Container fluid  -->
  <!-- ============================================================== -->
  <!-- End Container fluid  -->
  <!-- ============================================================== -->
  <!-- ============================================================== -->
  <!-- footer -->
  <!-- ============================================================== -->
  <footer class="footer"> <?php $this->load->view('plantilla/footer'); ?></footer>
  <!-- ============================================================== -->
  <!-- End footer -->
  <!-- ============================================================== -->
</div>


<!-- ============================================================== -->
<!-- End Page wrapper  -->
<!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Wrapper -->
<!-- ============================================================== -->






<?php $this->load->view('plantilla/javascript'); ?>




</body>

</html>