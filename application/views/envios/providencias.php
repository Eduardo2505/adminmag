<!DOCTYPE html>
<html lang="en">

<head>

  <?php $this->load->view('plantilla/head'); ?>

</head>

<body class="fix-header card-no-border fix-sidebar">
  <!-- ============================================================== -->
  <!-- Preloader - style you can find in spinners.css -->
  <!-- ============================================================== -->
  <div class="preloader">
    <div class="loader">
      <div class="loader__figure"></div>
      <p class="loader__label">Apilink Systems</p>
    </div>
  </div>
  <!-- ============================================================== -->
  <!-- Main wrapper - style you can find in pages.scss -->
  <!-- ============================================================== -->
  <div id="main-wrapper">
    <!-- ============================================================== -->
    <!-- Topbar header - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <header class="topbar">
     <?php echo $topbar?>
   </header>
   <aside class="left-sidebar">
    <?php echo $menu?>
  </aside>

  <!-- ============================================================== -->
  <!-- ============================================================== -->
  <!-- Page wrapper  -->
  <!-- ============================================================== -->
  <div class="page-wrapper">
    <!-- ============================================================== -->

    <!-- ============================================================== -->
    <div class="container-fluid">
      <!-- ============================================================== -->
      <!-- Bread crumb and right sidebar toggle -->
      <!-- ============================================================== -->
      <div class="row page-titles">
        <div class="col-md-5 align-self-center">
          <h3 class="text-themecolor">Providencias</h3>
        </div>
        <div class="col-md-7 align-self-center">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo site_url('') ?>">Inicio</a></li>
            <li class="breadcrumb-item">Consulta</li>
          </ol>
        </div>
            <!--<div class="">
                <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
              </div> -->
            </div>

            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Start Page Content -->
            <!-- ============================================================== -->
            <div class="row">
              <div class="col-md-12">


                <div class="card">
                  <div class="card-body">

                    <div id="education_fields"></div>
                    <div class="row">

                      <div class="col-sm-3 nopadding">
                        <div class="form-group">

                         <select id="regiones" name="regiones" class="form-control" required="" >
                          <option value="">Seleccione</option>

                          <?php
                          foreach($regiones as $cat)
                          {

                           ?>
                           <option value="<?php echo $cat->idState?>"> <?php echo $cat->state_name?>  </option>                        


                           <?php
                         }

                         ?>

                       </select>


                     </div>
                   </div>
                   <div class="col-sm-3 nopadding">
                    <div class="form-group">

                      <select name="comunas" id="comunas"  class="form-control" >
                        <option value="-1">Ninguno</option>


                      </select>


                    </div>
                  </div>



                  <div class="col-sm-6 nopadding">
                    <div class="form-group">
                      <a href="#add" id="addbtnt" class="btn btn-info">Agregar</a>
                      <a href="<?php echo site_url('') ?>envios"  class="btn btn-success">Regresar</a>
                      <a href="<?php echo site_url('') ?>envios/cragaMasiva?id=<?php echo $idcampania; ?>"  class="btn btn-warning">Carga Masiva</a>

                    </div>
                  </div>

                </div>
              </div>
            </div>

          </div>
        </div>


        <?php
        if (isset($sProvince)) {

          $rowcount=$sProvince->num_rows();
          if($rowcount!=0){

            ?>
            <div class="row">
              <form action="<?php echo site_url('') ?>envios/elimiarProvi" method="POST">
                <input type="hidden" name="idcampania" value="<?php echo $idcampania;?>">

                <div class="col-lg-12">
                  <div class="card">
                    <div class="card-body">


                      <div class="table-responsive">

                       <table id="tableCompras" class="table">
                        <thead>
                         <thead>
                          <tr>
                         <!--  <th>idcampania_regiones_wdsl</th> 
                          <th>idProvince</th>
                          <th>id</th> -->
                          <th>Eliminar <!-- <input type="checkbox" class="check checkInfinito" id="todoslods"   data-checkbox="icheckbox_flat-red">
                            <label for="todoslods"></label> --></th> 
                            <th>Región</th>
                            <th>Comuna</th>
                            <th>precio</th> 
                            <th>precio_extra</th> 

                          </tr>
                        </thead>
                        <?php
                        foreach ($sProvince->result() as $rowx) {
                          ?>


                          <tr>

                           <td> <input type="checkbox" class="check checkInfinito" id="idche<?php echo $rowx->idcampania_regiones_wdsl;?>" value="<?php echo $rowx->idcampania_regiones_wdsl;?>" name="ideliminar[]" data-checkbox="icheckbox_flat-red">
                            <label for="idche<?php echo $rowx->idcampania_regiones_wdsl;?>"></label> </td> 
                            <td><?php echo $rowx->state_name; ?></td>
                            <td><?php echo $rowx->procince_name; ?></td>

                            

                            <td><input type="number" class="form-control precio" title="<?php echo $rowx->idcampania_regiones_wdsl;?>" value="<?php echo $rowx->precio; ?>"></td>
                            <td><input type="number" class="form-control precioextra" title="<?php echo $rowx->idcampania_regiones_wdsl;?>" value="<?php echo $rowx->precio_extra; ?>" ></td>




                          </tr>



                          <?php
                        }

                        ?>



                      </table>


                    </div>

                  </div>
                  <input type="submit" value="Eliminar" class="btn btn-info btn-rounded m-t-10 float-right" >

                </div>
              </div>

            </form>

          </div>
          <?php
        }
      }
      ?>   





      <!-- ============================================================== -->
      <!-- End PAge Content -->
      <!-- ============================================================== -->
      <!-- ============================================================== -->
      <!-- Right sidebar -->
      <!-- ============================================================== -->
      <!-- .right-sidebar -->

      <!-- ============================================================== -->
      <!-- End Right sidebar -->
      <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- footer -->
    <!-- ============================================================== -->
    <footer class="footer"> <?php $this->load->view('plantilla/footer'); ?></footer>
    <!-- ============================================================== -->
    <!-- End footer -->
    <!-- ============================================================== -->
  </div>


  <!-- ============================================================== -->
  <!-- End Page wrapper  -->
  <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Wrapper -->
<!-- ============================================================== -->






<?php $this->load->view('plantilla/javascript'); ?>

<script>
  jQuery(document).ready(function() {

    $(".precioextra").focusout(function(e) {


     $(".preloader").fadeIn();
     var url = "<?php echo site_url('') ?>envios/udateprecioExtra";
     var id=$(this).attr('title');
     let valor=$(this).val();
     if(valor==""){
      valor=0;
    }
    var dataenvio="id="+id+"&valor="+valor;
    //console.log(dataenvio);

    $.ajax({
     type: "GET",
     url: url,
     data: dataenvio, 
     success: function(data)

     {
      $(".preloader").fadeOut();



    }
  });
    e.preventDefault(); 
  });


    $(".precio").focusout(function(e) {


     $(".preloader").fadeIn();
     var url = "<?php echo site_url('') ?>envios/udateCatidad";
     var id=$(this).attr('title');
     let valor=$(this).val();
     if(valor==""){
      valor=0;
    }
    var dataenvio="id="+id+"&valor="+valor;
    //console.log(dataenvio);

    $.ajax({
     type: "GET",
     url: url,
     data: dataenvio, 
     success: function(data)

     {
      $(".preloader").fadeOut();



    }
  });
    e.preventDefault(); 
  });


    $("#regiones").change(function(e) {


     $(".preloader").fadeIn();
     var url = "<?php echo site_url('') ?>envios/getComunas";
     var id=$("#regiones").val();
     var dataenvio="id="+id;

     $.ajax({
       type: "GET",
       url: url,
       data: dataenvio, 
       success: function(data)

       {


         $('#comunas').html(data);
         $(".preloader").fadeOut();




       }
     });
     e.preventDefault(); 
   });

    $("#addbtnt").click(function(e) {


     $(".preloader").fadeIn();
     var url = "<?php echo site_url('') ?>envios/agergarProvidencias";
     var idregiones=$("#regiones").val();
     var idcomunas=$("#comunas").val();
     var nomregion=$("#regiones option:selected" ).text();
     var dnomcomunas=$("#comunas option:selected" ).text();
     var idcampania=<?php echo $idcampania; ?>;

     var dataenvio="idcampania="+idcampania
     +"&idregiones="+idregiones
     +"&idcomunas="+idcomunas
     +"&nomregion="+nomregion.trim()
     +"&dnomcomunas="+dnomcomunas.trim();
     console.log(url+"?"+dataenvio);
     $.ajax({
       type: "GET",
       url: url,
       data: dataenvio, 
       success: function(data)

       {


   //$('#comunas').html(data);
   //$(".preloader").fadeOut();
   $(location).attr('href','<?php echo site_url('') ?>envios/providencias?id='+idcampania);



 }
});
     e.preventDefault(); 
   });

  });
</script>


</body>

</html>