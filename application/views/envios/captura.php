<!DOCTYPE html>
<html lang="en">

<head>
  <link rel="stylesheet" href="<?php echo site_url('') ?>assets/plugins/venobox/venobox.css" type="text/css" media="screen" />
  <?php $this->load->view('plantilla/head'); ?>

  <link href="<?php echo site_url('') ?>assets/plugins/multiselect/css/multi-select.css" rel="stylesheet" type="text/css" />

</head>

<body class="fix-header card-no-border fix-sidebar">
  <!-- ============================================================== -->
  <!-- Preloader - style you can find in spinners.css -->
  <!-- ============================================================== -->
  <div class="preloader">
    <div class="loader">
      <div class="loader__figure"></div>
      <p class="loader__label">Apilink Systems</p>
    </div>
  </div>
  <!-- ============================================================== -->
  <!-- Main wrapper - style you can find in pages.scss -->
  <!-- ============================================================== -->
  <div id="main-wrapper">
    <!-- ============================================================== -->
    <!-- Topbar header - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <header class="topbar">
     <?php echo $topbar?>
   </header>
   <aside class="left-sidebar">
    <?php echo $menu?>
  </aside>

  <!-- ============================================================== -->
  <!-- ============================================================== -->
  <!-- Page wrapper  -->
  <!-- ============================================================== -->
  <div class="page-wrapper">
    <!-- ============================================================== -->

    <!-- ============================================================== -->
    <div class="container-fluid">
      <!-- ============================================================== -->
      <!-- Bread crumb and right sidebar toggle -->
      <!-- ============================================================== -->
      <div class="row page-titles">
        <div class="col-md-5 align-self-center">
          <h3 class="text-themecolor">Cupones</h3>
        </div>
        <div class="col-md-7 align-self-center">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo site_url('') ?>">Inicio</a></li>
            <li class="breadcrumb-item">Captura</li>
          </ol>
        </div>
            <!--<div class="">
                <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
              </div> -->
            </div>



            <form id="form" >



              <div class="row">

                <div class="col-lg-4">
                  <div class="card card-body">
                    <h4 class="card-title">Envios</h4>


                    <div class="form-group">
                      <label>Nombre</label>
                      <input required="" type="text" class="form-control" name="campania">
                    </div>


                    <div class="form-group">
                      <label>Estatus</label>
                      <select name="estatus" id="estatus" class="form-control" >
                        <option value="1"> Activo  </option>                        
                        <option value="0" >Incativo</option>


                      </select>
                    </div>
                    <div class="form-group">

                      <input type="submit" class="btn btn-info btn-rounded m-t-10 " value="Enviar">
                      <a  href="<?php echo site_url('') ?>envios" class="btn btn-warning btn-rounded m-t-10 float-right" >Regresar</a>
                    </div>


                  </div>
                </div>


              </div>




            </form>

          </div>

        </div>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Right sidebar -->
        <!-- ============================================================== -->
        <!-- .right-sidebar -->

        <!-- ============================================================== -->
        <!-- End Right sidebar -->
        <!-- ============================================================== -->
      </div>
      <!-- ============================================================== -->
      <!-- End Container fluid  -->
      <!-- ============================================================== -->
      <!-- End Container fluid  -->
      <!-- ============================================================== -->
      <!-- ============================================================== -->
      <!-- footer -->
      <!-- ============================================================== -->
      <footer class="footer"> <?php $this->load->view('plantilla/footer'); ?></footer>
      <!-- ============================================================== -->
      <!-- End footer -->
      <!-- ============================================================== -->
    </div>


    <!-- ============================================================== -->
    <!-- End Page wrapper  -->
    <!-- ============================================================== -->
  </div>
  <!-- ============================================================== -->
  <!-- End Wrapper -->
  <!-- ============================================================== -->





  <?php $this->load->view('plantilla/javascript'); ?>

  <script type="text/javascript" src="<?php echo site_url('') ?>assets/plugins/multiselect/js/jquery.multi-select.js"></script>
  <script>
    jQuery(document).ready(function() {


      $("#form").submit(function(e) {


       var url = "<?php echo site_url('') ?>envios/guardar";
       $.ajax({
         type: "POST",
         url: url,
         data: $("#form").serialize(), 
         success: function(data)

         {

           swal({
            title: 'Exito!',
            text: "Se guardo correctamen",
            type: 'success'
          })
           .then((result)=>{
            $(location).attr('href','<?php echo site_url('') ?>envios');
          })





         }
       });
       e.preventDefault(); 
     });

    });
  </script>


</body>

</html>