<!DOCTYPE html>
<html lang="en">

<head>

    <?php $this->load->view('plantilla/head'); ?>

</head>

<body class="fix-header card-no-border fix-sidebar">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">Apilink Systems</p>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar">
         <?php echo $topbar?>
     </header>
     <aside class="left-sidebar">
        <?php echo $menu?>
    </aside>

    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Page wrapper  -->
    <!-- ============================================================== -->
    <div class="page-wrapper">
        <!-- ============================================================== -->

        <!-- ============================================================== -->
        <div class="container-fluid">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor">Key</h3>
                </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo site_url('') ?>">Inicio</a></li>
                        <li class="breadcrumb-item">Consulta</li>
                    </ol>
                </div>
            <!--<div class="">
                <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
            </div> -->
        </div>

        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-md-12">
                <form action="<?php echo site_url('') ?>cupones/key" method="G">
                    <div class="card">
                        <div class="card-body">

                            <div id="education_fields"></div>
                            <div class="row">

                                <div class="col-sm-8 nopadding">
                                    <div class="form-group">
                                     <input type="text" class="form-control" name="filtro" placeholder="Buscar">
                                 </div>
                             </div>

                             <div class="col-sm-3 nopadding">
                                <div class="form-group">
                                    <input type="submit" class="btn btn-info"  value="Buscar">
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="row">

        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">

                    <div class="table-responsive">

                     <table id="tableCompras" class="table">
                        <thead>
                            <tr>
                                <th>Key</th>
                                <th>Estatus</th>
                                <th>Cliente</th>
                                <th>Referencia</th>
                                <th>Campaña</th>                               
                                <th>Registro</th>

                            </tr>
                        </thead>
                        <tbody>

                            <?php
                            if (isset($registros)) {
                                foreach ($registros->result() as $rowx) {
                                    ?>


                                    <tr>
                                        <td><?php echo $rowx->idkey_cupones;?></td>
                                        <td><?php echo $rowx->estado;?></td>
                                        <td><?php echo $rowx->cliente; ?></td>
                                        <td><?php echo $rowx->referencia; ?></td>
                                        <td><?php echo $rowx->campania; ?></td>

                                        <td><?php echo $rowx->registro; ?></td>
                                        

                                    </tr>



                                    <?php
                                }
                            }
                            ?>   




                        </tbody>
                    </table>


                </div>
                <nav>
                    <?php echo $pagination; ?>  
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End PAge Content -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Right sidebar -->
<!-- ============================================================== -->
<!-- .right-sidebar -->

<!-- ============================================================== -->
<!-- End Right sidebar -->
<!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- footer -->
<!-- ============================================================== -->
<footer class="footer"> <?php $this->load->view('plantilla/footer'); ?></footer>
<!-- ============================================================== -->
<!-- End footer -->
<!-- ============================================================== -->
</div>


<!-- ============================================================== -->
<!-- End Page wrapper  -->
<!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Wrapper -->
<!-- ============================================================== -->






<?php $this->load->view('plantilla/javascript'); ?>




</body>

</html>