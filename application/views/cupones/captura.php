<!DOCTYPE html>
<html lang="en">

<head>
    <link rel="stylesheet" href="<?php echo site_url('') ?>assets/plugins/venobox/venobox.css" type="text/css" media="screen" />
    <?php $this->load->view('plantilla/head'); ?>

    <link href="<?php echo site_url('') ?>assets/plugins/multiselect/css/multi-select.css" rel="stylesheet" type="text/css" />

</head>

<body class="fix-header card-no-border fix-sidebar">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">Apilink Systems</p>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar">
         <?php echo $topbar?>
     </header>
     <aside class="left-sidebar">
        <?php echo $menu?>
    </aside>

    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Page wrapper  -->
    <!-- ============================================================== -->
    <div class="page-wrapper">
        <!-- ============================================================== -->

        <!-- ============================================================== -->
        <div class="container-fluid">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor">Cupones</h3>
                </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo site_url('') ?>">Inicio</a></li>
                        <li class="breadcrumb-item">Captura</li>
                    </ol>
                </div>
            <!--<div class="">
                <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
            </div> -->
        </div>



        <form id="form" >



            <div class="row">

                <div class="col-lg-4">
                    <div class="card card-body">
                        <h4 class="card-title">Campaña</h4>


                        <div class="form-group">
                            <label>Referencia</label>
                            <input required="" type="text" class="form-control" name="referencia">
                        </div>
                        <div class="form-group">
                            <label>Campaña</label>
                            <input type="text" required="" class="form-control" name="campania">
                        </div>
                        <div class="form-group">
                            <label>Inicio</label>
                            <input type="date" class="form-control" name="inicio" required="">
                        </div>
                        <div class="form-group">
                            <label>Final</label>
                            <input type="date" class="form-control" name="final" required="">
                        </div>

                        <div class="form-group">
                            <label>Tipo</label>
                            <select name="tipo" id="tipo" class="form-control" >
                                <option value="1"> Producto  </option>                        
                                <option value="2" >Categoría</option>


                            </select>
                        </div>



                    </div>
                </div>

                <div class="col-lg-8">
                   <div class="col-lg-12">
                    <div class="card card-body">
                        <h4 class="card-title" id ="titlecategoria" style="display: none">Categoría</h4>
                        <h4 class="card-title" id ="titleProducto" >Productos</h4>

                        <div class="form-group" id ="cateOcultar">
                            <label>Categoría</label>
                            <select name="categoria" id="Cahangecategoria" class="form-control" >
                                <option value="-1">Todos</option>                        
                                <?php 

                                if (isset($resultado)) {
                                    foreach ($resultado->result() as $rowx) {
                                        ?>
                                        <option value="<?php echo $rowx->IdCategory ?>"><?php echo $rowx->NameCategory; ?></option>


                                        <?php
                                    }
                                }
                                ?>

                            </select>
                        </div>

                        <select class="form-group" multiple id="public-methods" name="productosOcategorias[]">
                         <?php 

                         if (isset($productos)) {
                            foreach ($productos->result() as $rowx) {
                                ?>
                                <option value="<?php echo $rowx->ProductReference ?>"> <?php echo $rowx->ProductReference ?> </option>


                                <?php
                            }
                        }
                        ?>



                    </select>
                    <div class="button-box m-t-20"> 
                        <a id="select-all" class="btn btn-danger" href="#">Todos</a> 
                        <a id="deselect-all" class="btn btn-info" href="#">Eliminar</a> 

                    </div>

                </div>
            </div>
            <div class="col-lg-12">
                <div class="card card-body">
                    <h4 class="card-title">Configuraciones</h4>


                    <div class="form-group">
                        <label>Cantidad</label>
                        <input required=""  type="number" max="99999999" class="form-control" name="cantidad">
                    </div>
                    <div class="form-group">
                        <label>Prefijo</label>
                        <input type="text" maxlength="10" class="form-control" name="prefijo" required="" >
                    </div>
                    <div class="form-group">
                        <label>Cantidades Utilizadas</label>
                        <input type="number" max="99999999"  class="form-control" id="cantidad" name="cantidadutlizados"><br><br>
                        <input type="checkbox" class="check checkInfinito" id="flat-checkbox-3" name="infinito" data-checkbox="icheckbox_flat-red">
                        <label for="flat-checkbox-3">Infinito</label>
                    </div>





                </div>
            </div>

            <div class="col-lg-12">
                <div class="card card-body">

                    <div class="form-group">
                        <label>Descuento valor por : </label>
                        <select id="selectValorDescuento" name="selectValorDescuento" class="form-control" >
                         <option value="-1">Seleccione</option>
                         <option value="1">OFF</option>                        
                         <option value="2">Porcentaje</option>


                     </select>
                 </div>
                 <div class="form-group">

                    <input type="number" class="form-control" id="valorDescuento" name="valorDescuento">
                </div>




            </div>
        </div>

        <div class="col-lg-12">
            <div class="card card-body">

                <div class="form-group">
                    <label>Descuento de envió por : </label>
                    <select id="selectValorEnvio" name="selectValorEnvio"  class="form-control" >
                        <option value="-1">Seleccione</option>    
                        <option value="1">Valor</option>                        
                        <option value="2">Porcentaje</option>


                    </select>
                </div>
                <div class="form-group">

                    <input type="number" class="form-control" id="valorEnvio" name="valorEnvio">
                </div>




            </div>
        </div>
        
        <a  href="<?php echo site_url('') ?>cupones" class="btn btn-warning btn-rounded m-t-10 float-right" >Regresar</a></button>
        <input type="submit" class="btn btn-info btn-rounded m-t-10 " value="Enviar">
    </form>

</div>

</div>
<!-- ============================================================== -->
<!-- End PAge Content -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Right sidebar -->
<!-- ============================================================== -->
<!-- .right-sidebar -->

<!-- ============================================================== -->
<!-- End Right sidebar -->
<!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- footer -->
<!-- ============================================================== -->
<footer class="footer"> <?php $this->load->view('plantilla/footer'); ?></footer>
<!-- ============================================================== -->
<!-- End footer -->
<!-- ============================================================== -->
</div>


<!-- ============================================================== -->
<!-- End Page wrapper  -->
<!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Wrapper -->
<!-- ============================================================== -->





<?php $this->load->view('plantilla/javascript'); ?>

<script type="text/javascript" src="<?php echo site_url('') ?>assets/plugins/multiselect/js/jquery.multi-select.js"></script>

<script>
    jQuery(document).ready(function() {


        $("#form").submit(function(e) {

        // $(".preloader").fadeIn();
        var selections = [];
        $("#public-methods option:selected").each(function(){
         var optionValue = $(this).val();
         var optionText = $(this).text();
         selections.push(optionValue);
     });

        if(selections.length==0){

            alert("Debe de seleccionar por lo menos un producto/categoría");
            return false ;
        }


        var valorchec=$('#flat-checkbox-3').is(':checked');

        if(!valorchec) {
            var string=$("#cantidad").val();
            var cantid=string.length;                
            if(cantid==0){
                alert("Capture la cantidad a utilizar");
                return false ;
            }
        }

        var selecvdes=$('#selectValorDescuento').val();
        var selecvdesenvio=$('#selectValorEnvio').val();


        if(selecvdes==-1&&selecvdesenvio==-1){
            alert(" Seleccione un tipo de descuento");
            return false ;
        }

        if(selecvdes!=-1){

            var string=$("#valorDescuento").val().length;
            if(string==0){
                alert(" Seleccione un valor al descuento");
                return false ;
            }
        }
        if(selecvdesenvio!=-1){

            var string=$("#valorEnvio").val().length;
            if(string==0){
             alert(" Capture un valor al envió");
             return false ;
         }
     }






     var url = "<?php echo site_url('') ?>cupones/guardar";
     $.ajax({
       type: "POST",
       url: url,
       data: $("#form").serialize(), 
       success: function(data)

       {

         swal({
            title: 'Exito!',
            text: "Los codigos se generaron correctamente",
            type: 'success'
        })
         .then((result)=>{
            $(location).attr('href','<?php echo site_url('') ?>cupones/captura');
        })





     }
 });
     e.preventDefault(); 
 });



        $(".checkInfinito").change(function(e) {
           if(this.checked) {
             var id=$("#cantidad").val("");
             $("#cantidad").prop( "disabled", true );
         }else{
             var id=$("#cantidad").val("");
             $("#cantidad").prop( "disabled", false );
         }
     });


        $('#public-methods').multiSelect();
        $('#select-all').click(function() {
            $('#public-methods').multiSelect('select_all');
            return false;
        });
        $('#deselect-all').click(function() {
            $('#public-methods').multiSelect('deselect_all');
            return false;
        });

        


        $("#Cahangecategoria").change(function(e) {

           $(".preloader").fadeIn();

           var url = "<?php echo site_url('') ?>cupones/productosCate";
           var id=$("#Cahangecategoria").val();
           var dataenvio="IdCategory="+id;

           $.ajax({
               type: "GET",
               url: url,
               data: dataenvio, 
               success: function(data)

               {

                var json=$('#public-methods').val();

                $("#public-methods").empty(); 

                $('#public-methods').append(data);

                $.each(json,function(key, value){
                 $('#public-methods').append('<option value="'+value+'" selected="selected">'+value+'</option>');
                }); 
                $(".preloader").fadeOut();
                $('#public-methods').multiSelect('refresh');





            }
        });
           e.preventDefault(); 
       });


        $("#tipo").change(function(e) {

           $(".preloader").fadeIn();

           $("#Cahangecategoria").val(-1); 

           var url = "<?php echo site_url('') ?>cupones/selectTipo";
           var tipo=$("#tipo").val();
           var dataenvio="tipo="+tipo;

           $.ajax({
               type: "GET",
               url: url,
               data: dataenvio, 
               success: function(data)

               {

                $(".preloader").fadeOut();


                $("#public-methods").empty(); 
                if(tipo==1){

                   $("#cateOcultar").css("display", "block");
                   $("#cateOcultarh4").css("display", "block");
                   $("#titleProducto").css("display", "block");
                   $("#titlecategoria").css("display", "none");

               }else{
                   $("#cateOcultar").css("display", "none");
                   $("#cateOcultarh4").css("display", "none");
                   $("#titleProducto").css("display", "none");
                   $("#titlecategoria").css("display", "block");

               }

               $('#public-methods').append(data);

               $('#public-methods').multiSelect('refresh');


           }
       });
           e.preventDefault(); 
       });



    });
</script>

</body>

</html>