<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');
//
class Envios_servicios extends CI_Model {
	private $class = 'Envios_servicios';
	function __construct() {
		parent::__construct();
		$this->load->helper('logs');
		$this->load->helper('date');
		$this->load->model('envios_models','dao');


	}

	public function insertarCampaniaCate($data) {

		$this->db->trans_begin();

		$this->dao->insertarCampaniaCate($data);

		logsError($this->db->error(),$this->class,'insertarCampaniaCate');

		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return -1;
		} else {
			$this->db->trans_commit();
		}

	}

	public function deleteCampaniaCate($idcampania) {

		$this->db->trans_begin();
		
		$this->dao->deleteCampaniaCate($idcampania);

		logsError($this->db->error(),$this->class,'deleteCampaniaCate');

		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return -1;
		} else {
			$this->db->trans_commit();
		}

	}

	public function getCategoriaCampania($idcampania) {

		$this->load->model('ccategoriawdsl_models','daocate');

		return $this->daocate->getCategoriaCampania($idcampania);

	}

	public function deleteCampaniaProductid($id,$categoria){

		$this->db->trans_begin();
		
		$this->dao->deleteCampaniaProductid($id,$categoria);

		logsError($this->db->error(),$this->class,'deleteCampaniaProductid');

		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return -1;
		} else {
			$this->db->trans_commit();
		}

	}


	public function getProductosCampania($idCategoria,$idcampania) {

		$this->load->model('cproductoswdsl_models','daov');

		return $this->daov->getProductosCampania($idCategoria,$idcampania);


	}

	public function deleteCampaniaProduct($idcampania) {

		$this->db->trans_begin();
		
		$this->dao->deleteCampaniaProduct($idcampania);

		logsError($this->db->error(),$this->class,'deleteCampaniaProduct');

		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return -1;
		} else {
			$this->db->trans_commit();
		}

	}

	public function insertarCampaniaProduct($data) {

		$this->db->trans_begin();

		$this->dao->insertarCampaniaProduct($data);

		logsError($this->db->error(),$this->class,'insertarCampaniaProduct');

		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return -1;
		} else {
			$this->db->trans_commit();
		}

	}

	public function getCategorias() {

		$this->load->model('ccategoriawdsl_models','daocate');

		return $this->daocate->get();

	}

	public function getProductos($idCategoria) {

		$this->load->model('cproductoswdsl_models','daov');

		return $this->daov->getProductos($idCategoria);


	}

	public function vertificaComuna($idcomunas,$idregiones,$idcampania) {

		

		return $this->dao->vertificaComuna($idcomunas,$idregiones,$idcampania);

	}
	public function getProvidenciascvs($ip,$id) {

		

		return $this->dao->getProvidenciascvs($ip,$id);

	}

	public function getProvidencias($ip,$id) {

		

		return $this->dao->getProvidencias($ip,$id);

	}

	public function insertarregionCampania($datos) {

		

		return $this->dao->insertarregionCampania($datos);

	}

	public function getcampanias($offset,$limite,$filtro) {

		

		return $this->dao->getcampanias($offset,$limite,$filtro);

	}
	public function deleteregion($id) {

		

		return $this->dao->deleteregion($id);

	}
	public function getcampaniascout($filtro) {

		

		return $this->dao->getcampaniascout($filtro);

	}

	public function insertar($datos) {

		

		return $this->dao->insertar($datos);

	}
	public function updateregion($id, $data){

		

		return $this->dao->updateregion($id, $data);

	}

	public function update($id, $data){

		

		return $this->dao->update($id, $data);
		

	}

	public function getId($id){

		

		return $this->dao->getId($id);

	}


	

	
	

	
}
?>
