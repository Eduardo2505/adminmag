<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');
//

class Admin_servicios extends CI_Model
{
	private $class = 'Admin_servicios';
	function __construct()
	{
		parent::__construct();
		$this->load->helper('logs');
		$this->load->helper('date');
		$this->load->model('cproductoswdsl_models', 'daopro');
	}

	public function getRegiones()
	{

		$cliente = new SoapClient(URL_WDSL, array('trace' => 1));

		$parametros = array(
			"key" => KEY,
			"secretKey" => SECRET_KEY,
			"country_ISO" => COUNTRY_ISO
		);


		$regiones = $cliente->getStatesByCountryISO($parametros);

		return $regiones->getStatesByCountryISOResult->worldStates;
	}

	public function getComunas($idState)
	{


		$cliente = new SoapClient(URL_WDSL, array('trace' => 1));
		$parametroscomu = array(
			"key" => KEY,
			"secretKey" => SECRET_KEY,
			"idState" => $idState
		);


		$provincias = $cliente->getProvincesByStateId($parametroscomu);

		return $provincias->getProvincesByStateIdResult->worldProvinces;
	}
	public function activarCron()
	{
		$this->daopro->crontab(1);
	}
	public function activarProcesando()
	{
		$this->daopro->crontab(2);
	}
	public function verificarCron()
	{
		return $this->daopro->crontabEstado();

	}

	public function getprod()
	{
		$cliente = new SoapClient(URL_WDSL, array('trace' => 1));
		$parametros = array(
			"key" => KEY,
			"secretKey" => SECRET_KEY,
			"lang" => LANG,
			"user_type" => USER_TYPE_ADMIN,
			"partner" => PARTNER,
			"country_iso" => COUNTRY_ISO
		);

		print "<pre>";
		print_r($parametros);
		print "</pre>";

		$categoryProducts = $cliente->GetCategoryProductsUser($parametros);

		print "<pre>";
		print_r($categoryProducts);
		print "</pre>";
	}

	public function sincronizarProdutos()
	{

		$this->db->trans_begin();
		$time = time();
		echo date("d-m-Y (H:i:s)", $time)."\n";

		$cliente = new SoapClient(URL_WDSL, array('trace' => 1));
		$parametros = array(
			"key" => KEY,
			"secretKey" => SECRET_KEY,
			"lang" => LANG,
			"user_type" => USER_TYPE_ADMIN,
			"partner" => PARTNER,
			"country_iso" => COUNTRY_ISO
		);
		$categoryProducts = $cliente->GetCategoryProductsUser($parametros);
		$prodReference = array();
		$prodcate = array();
		$catePro = array();
		/*print "<pre>";
		print_r($categoryProducts);
		print "</pre>";*/



		foreach ($categoryProducts->GetCategoryProductsUserResult->Category as $cat) {

			$objcate = array(
				'IdCategory' => $cat->IdCategory,
				'NameCategory' => $cat->NameCategory,
				'estatus' => 1,
				'RefCategory' => $cat->RefCategory
			);

			array_push($prodcate, $objcate);



			if (sizeof($cat->Details->Detail) == 1) {
				$det = $cat->Details->Detail;
				if (sizeof($cat->Details->Detail->Prices->ItemPrice) == 1) {
					$price = $cat->Details->Detail->Prices->ItemPrice->ExtraPrice;
					$prodRef = $cat->Details->Detail->ProductReference;
					$imgproductos ='';
					if (sizeof($cat->Details->Detail->PicPreviews->PicPreview) == 1) {
						$imgproductos ='||'.$cat->Details->Detail->PicPreviews->PicPreview->UrlOriginal;
						
					} else {
						//$UrlOriginal = $cat->Details->Detail->PicPreviews->PicPreview[0]->UrlOriginal;
						foreach ($cat->Details->Detail->PicPreviews->PicPreview as $img) {
						  $imgproductos.='||'.$img->UrlOriginal;
						}
					}
					$obj = array(
						'ProductReference' => $prodRef,
						'Name' => $det->Name,
						'Product_type' => $det->Product_type,
						'Description' => $det->Description,
						'Prices' => $price,
						'imgproductos' => $imgproductos

					);
					array_push($prodReference, $obj);

					$objcp = array(
						'ProductReference' => $prodRef,
						'IdCategory' => $cat->IdCategory
					);
					array_push($catePro, $objcp);
				} else {
					$price = $cat->Details->Detail->Prices->ItemPrice[0]->ExtraPrice;
					$prodRef = $cat->Details->Detail->ProductReference;
					$imgproductos ='';

					if (sizeof($cat->Details->Detail->PicPreviews->PicPreview) == 1) {
						$imgproductos = '||'.$cat->Details->Detail->PicPreviews->PicPreview->UrlOriginal;
						
					} else {
						
						foreach ($cat->Details->Detail->PicPreviews->PicPreview as $img) {
							$imgproductos.='||'.$img->UrlOriginal;
						  }
					}

					$obj = array(
						'ProductReference' => $prodRef,
						'Name' => $det->Name,
						'Product_type' => $det->Product_type,
						'Description' => $det->Description,
						'Prices' => $price,
						'imgproductos' => $imgproductos
					);

					array_push($prodReference, $obj);

					$objcp = array(
						'ProductReference' => $prodRef,
						'IdCategory' => $cat->IdCategory
					);
					array_push($catePro, $objcp);
				}
			} else {
				foreach ($cat->Details->Detail as $det) {
					if (sizeof($det->Prices->ItemPrice) == 1) {
						$price = $det->Prices->ItemPrice->ExtraPrice;

						$imgproductos = '';
						if (sizeof($det->PicPreviews->PicPreview) == 1) {
							$imgproductos = '||'.$det->PicPreviews->PicPreview->UrlOriginal;
					
						} else {
	
							foreach ($det->PicPreviews->PicPreview as $img) {
							
								$imgproductos.='||'.$img->UrlOriginal;
							  }
						}

						$prodRef = $det->ProductReference;

						$obj = array(
							'ProductReference' => $prodRef,
							'Name' => $det->Name,
							'Product_type' => $det->Product_type,
							'Description' => $det->Description,
							'Prices' => $price,
							'imgproductos' => $imgproductos
						);

						array_push($prodReference, $obj);

						$objcp = array(
							'ProductReference' => $prodRef,
							'IdCategory' => $cat->IdCategory
						);
						array_push($catePro, $objcp);
					} else {
						$price = $det->Prices->ItemPrice[0]->ExtraPrice;
						$prodRef = $det->ProductReference;
						$imgproductos = '';
						if (sizeof($det->PicPreviews->PicPreview) == 1) {
							$imgproductos = '||'.$det->PicPreviews->PicPreview->UrlOriginal;
					
						} else {
						
							foreach ($det->PicPreviews->PicPreview as $img) {
								$imgproductos.='||'.$img->UrlOriginal;
							  }
						}

						$obj = array(
							'ProductReference' => $prodRef,
							'Name' => $det->Name,
							'Product_type' => $det->Product_type,
							'Description' => $det->Description,
							'Prices' => $price,
							'imgproductos' => $imgproductos
						);
						array_push($prodReference, $obj);
						$objcp = array(
							'ProductReference' => $prodRef,
							'IdCategory' => $cat->IdCategory
						);
						array_push($catePro, $objcp);
					}
				}
			}
		}

		
		$this->load->model('ccategoriawdsl_models', 'daocate');

		$prodcateaux = array_unique($prodcate, SORT_REGULAR);


		//Actualizar categorias
		$this->daocate->updatestado(0);
		foreach ($prodcateaux as $item) {

			$objp = array(
				'IdCategory' => $item['IdCategory'],
				'NameCategory' => $item['NameCategory']."_",
				'estado' => 1,
				'RefCategory' => $item['RefCategory']
			);
			$ver = $this->daocate->getIdCategory($item['IdCategory']);
			if ($ver != 0) {
				$this->daocate->update($item['IdCategory'], $objp);
			} else {

				$this->daocate->insertar($objp);
			}
		}


		$resultado = $this->daocate->getCategorias(1);
		$insert = "";
		$updaet = "";
		if (isset($resultado)) {
			foreach ($resultado->result() as $rowx) {
				$IdCategory = $rowx->IdCategory;
				$id_wp = $rowx->id_wp;
				$NameCategory = str_replace('"', '\"', $rowx->NameCategory);

				if ($id_wp != -1) {
					$updaet .= '{
						"id": ' . $id_wp . ',
						"description": "' . $IdCategory . '",
						"name": "' . $NameCategory . '"
					},';
				} else {
					$insert .= '{
						"name": "' . $NameCategory . '",
						"description": "' . $IdCategory . '"
					},';
				}
			}
		}

		$resultado = $this->daocate->getCategorias(0);
		$delete = "";
		if (isset($resultado)) {
			foreach ($resultado->result() as $rowx) {
				$id_wp = $rowx->id_wp;
				if ($id_wp != -1) {
					$delete .= '' . $id_wp . ',';
				}
			}
		}

		$json = '{
			"create": [
				' . $insert . '|||
			],
			"update": [
				' . $updaet . '|||
			],
			"delete": [
				' . $delete . '|||
			]
		}';
		$jsonclear = str_replace(",|||", "", $json);
		$jsonclearvacios = str_replace("|||", "", $jsonclear);

		


		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => URL_API_WP . "products/categories/batch",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POSTFIELDS => $jsonclearvacios,
			CURLOPT_HTTPHEADER => array(
				"Authorization: Basic " . KEY_API_WP,
				"Content-Type: application/json"
			),
		));
		$response = curl_exec($curl);

		curl_close($curl);


		$obj = json_decode($response);
		//print "<pre>";
		//print_r($obj);
		//print "</pre>";

		if (isset($obj->create)) {
			foreach ($obj->create as $det) {

				$this->daocate->updatewd($det->description, $det->id);
			}
		}
		if (isset($obj->update)) {
			foreach ($obj->update as $det) {

				//	echo ">>> uodate" . $det->id . "<br>";
			}
		}

		if (isset($obj->delete)) {

			foreach ($obj->delete as $det) {

				//	echo ">>> uodate" . $det->id . "<br>";
			}
		}




		$prodReferenceaux = array_unique($prodReference, SORT_REGULAR);

		

		$this->daopro->updatestado(0);

		foreach ($prodReferenceaux as $item) {

			$objp = array(
				'ProductReference' => $item['ProductReference'],
				'Name' => $item['Name'],
				'Product_type' => $item['Product_type'],
				//'url_img' => $item['UrlOriginal'],
				'descripcion' => $item['Description'],
				'precio' => $item['Prices'],
				'estado' => 1
			);


			$ver = $this->daopro->getreferencia($item['ProductReference']);
			if ($ver != 0) {
				$this->daopro->update($item['ProductReference'], $objp);
			} else {
				$this->daopro->insertar($objp);
			}
			$this->daopro->deleteImgProd($item['ProductReference']);
		
			if ($tags = explode('||',$item['imgproductos'])) { 
				foreach($tags as $key) { 
					if($key!=''){
						$obpcaux = array(
							'ProductReference' => $item['ProductReference'],
							'url_img' => $key
						); 
						$this->daopro->insertarImgProd($obpcaux);  

					}   
				}
			}
		
		
			
		}
		$this->daocate->deletecp();
		$cateProaux = array_unique($catePro, SORT_REGULAR);
		foreach ($cateProaux as $itemx) {

			$obpcaux = array(
				'ProductReference' => $itemx['ProductReference'],
				'IdCategory' => $itemx['IdCategory']
			);
			$this->daocate->insertarpc($obpcaux);
		}

		//Eliminar
		$totalreg = $this->daopro->getProductosestadocount(0);
		$pagc =5;
		$paguax = ceil($totalreg / $pagc);
		for ($i = 0; $i < $paguax; $i++) {
			$pag = $i * $pagc;
			echo "ESTADO 0\n";
			$resultado = $this->daopro->getProductosestado(0, $pag,  $pagc);
			$delete = "";
			if (isset($resultado)) {
				foreach ($resultado->result() as $rowx) {
					$idproductowp = $rowx->idproductowp;
					if ($idproductowp != -1) {
						$delete .= '' . $idproductowp . ',';
					}
				}
			}

			$json = '{
			"create": [
				' . $insert . '|||
			],
			"update": [
				' . $updaet . '|||
			],
			"delete": [
				' . $delete . '|||
			]
		}';
			$jsonclear = str_replace(",|||", "", $json);
			$jsonclearvacios = str_replace("|||", "", $jsonclear);
			$curl = curl_init();

			curl_setopt_array($curl, array(
				CURLOPT_URL => URL_API_WP . "products/batch",
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => "",
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 0,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => "POST",
				CURLOPT_POSTFIELDS => $jsonclearvacios,
				CURLOPT_HTTPHEADER => array(
					"Authorization: Basic " . KEY_API_WP,
					"Content-Type: application/json"
				),
			));

			$response = curl_exec($curl);
			curl_close($curl);
		
		}


		$totalreg = $this->daopro->getProductosestadocount(1);
		echo "TOTAL PRODUCTOS" . $totalreg . "\n";
		echo "TOTAL PAG" . $pagc . "\n";
		$paguax = ceil($totalreg / $pagc);
		echo "Paginas" . $paguax . "\n";
		$produpdate = array();
		for ($i = 0; $i < $paguax; $i++) {
			echo "ESTADO 1\n";
		
			$pag = $i * $pagc;
			echo "Paginasnacion " . $pag." ==== ". $pagc . "\n";
			$resultado = $this->daopro->getProductosestado(1, $pag, $pagc);
			$insert = "";
			$updaet = "";
			if (isset($resultado)) {
				foreach ($resultado->result() as $rowx) {
					$productReference = $rowx->productReference;
					$idproductowp = $rowx->idproductowp;
					$idcategoriawp = $rowx->idcategoriawp;
					$getImagenes = $this->daopro->getImagenes($productReference);
					$imgjson='';
					foreach ($getImagenes->result() as $rowi) {
						$imgjson.=',{
							"src": "' . $rowi->url_img . '"
						}';
					}
					
					$cleanimg=substr($imgjson, 1); 
					
					$namepro = $rowx->namepro;
					$descripcion = str_replace('"', '\"', $rowx->descripcion);
					$precio = $rowx->precio;
					if ($idproductowp != -1) {
						$updaet .= '{
						"id": ' . $idproductowp . ',
						"name": "_' . $namepro . '",
						"status":"draft",
						"regular_price": "' . $precio . '",
						"slug": "' . $productReference . '",
						"short_description": "' . $descripcion . '",
						"categories": [
							{
								"id": ' . $idcategoriawp . '
							}
						],
						"images": [
							'.$cleanimg.'
						],
						"attributes": [
							{
								"id": 0,
								"name": "productReference",
								"position": 0,
								"visible": true, 
								"variation": false,
								"options": [
									"' . $productReference . '"
								]
							}
						]
					},';
					} else {
						$insert .= '{
						"name": "_' . $namepro . '",
						"type": "simple",
						"status":"draft",
						"regular_price": "' . $precio . '",
						"slug": "' . $productReference . '",
						"short_description": "' . $descripcion . '",
						"categories": [
							{
								"id": ' . $idcategoriawp . '
							}
						],
						"images": [
							'.$cleanimg.'
						],
						"attributes": [
							{
								"id": 0,
								"name": "productReference",
								"position": 0,
								"visible": true,
								"variation": false,
								"options": [
									"' . $productReference . '"
								]
							}
						]
					},';
					}
				}
			}

			$json = '{
			"create": [
				' . $insert . '|||
			],
			"update": [
				' . $updaet . '|||
			],
			"delete": [
				' . $delete . '|||
			]
		}';
			$jsonclear = str_replace(",|||", "", $json);
			$jsonclearvacios = str_replace("|||", "", $jsonclear);
         
			$curl = curl_init();

			curl_setopt_array($curl, array(
				CURLOPT_URL => URL_API_WP . "products/batch",
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => "",
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 0,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => "POST",
				CURLOPT_POSTFIELDS => $jsonclearvacios,
				CURLOPT_HTTPHEADER => array(
					"Authorization: Basic " . KEY_API_WP,
					"Content-Type: application/json"
				),
			));

			$response = curl_exec($curl);
			curl_close($curl);
			$obj = json_decode($response);

			if (isset($obj->create)) {

				foreach ($obj->create as $det) {
					$ref = $det->attributes[0]->options[0];

					$obj = array(
						'ProductReference' => $ref,
						'id_wp' => $det->id
					);

					array_push($produpdate, $obj);

					//$this->daopro->updatewd($ref, $det->id);
				}
			}

			
			sleep(2);
		// break;
			
		}


		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return -1;
			//echo "ERROR";
		} else {
			$this->db->trans_commit();
			//echo "Ejecuto";
		}


		

		$this->daopro->update_batch($produpdate);
		$this->daopro->crontab(0);
		$time = time();
		echo date("d-m-Y (H:i:s)", $time)."\n";
		echo " 1.-Ejcuto update_batch \n";

	
		
	}

	public function clearProductos($totalreg)
	{

		$paguax = ceil($totalreg / 10);
		for ($i = 0; $i < $paguax; $i++) {


			$curl = curl_init();

			curl_setopt_array($curl, array(
				CURLOPT_URL => URL_API_WP . "products",
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => "",
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 0,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => "GET",
				CURLOPT_HTTPHEADER => array(
					"Authorization: Basic " . KEY_API_WP
				),
			));

			$response = curl_exec($curl);

			curl_close($curl);
			$obj = json_decode($response);
			$delete = "";



			if (isset($obj)) {

				foreach ($obj as $det) {

					$delete .= '' . $det->id . ',';
				}
			}

			$json = '{
			"create": [
			],
			"update": [
			],
			"delete": [
				' . $delete . '|||
			]
		}';
			$jsonclear = str_replace(",|||", "", $json);
			$jsonclearvacios = str_replace("|||", "", $jsonclear);





			$curl = curl_init();

			curl_setopt_array($curl, array(
				CURLOPT_URL => URL_API_WP . "products/batch",
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => "",
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 0,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => "POST",
				CURLOPT_POSTFIELDS => $jsonclearvacios,
				CURLOPT_HTTPHEADER => array(
					"Authorization: Basic " . KEY_API_WP,
					"Content-Type: application/json"
				),
			));

			$response = curl_exec($curl);

			curl_close($curl);
			echo "DELETE CICLE " . $i . "\n";
		}
	}
}
