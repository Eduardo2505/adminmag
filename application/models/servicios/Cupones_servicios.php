<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');
//
class Cupones_servicios extends CI_Model {
	private $class = 'Cupones_servicios';
	function __construct() {
		parent::__construct();
		$this->load->helper('logs');
		$this->load->helper('date');


	}

	public function getProducts($id,$tipo) {

		$this->load->model('cupones_wdsl_models','cudao');

		return $this->cudao->getProducts($id,$tipo);

	}

	public function getId($id) {

		$this->load->model('cupones_wdsl_models','cudao');

		return $this->cudao->getId($id);

	}

	public function getkeybus($offset,$limite,$filtro) {

		$this->load->model('cupones_wdsl_models','dao');

		return $this->dao->getkeybus($offset,$limite,$filtro);

	}
	public function getkeybuscount($filtro) {

		$this->load->model('cupones_wdsl_models','dao');

		return $this->dao->getkeybuscount($filtro);

	}

	public function getCampanias($offset,$limite,$filtro) {

		$this->load->model('cupones_wdsl_models','dao');

		return $this->dao->getCampanias($offset,$limite,$filtro);

	}
	public function getCampaniascount($filtro) {

		$this->load->model('cupones_wdsl_models','dao');

		return $this->dao->getCampaniascount($filtro);

	}

	public function getkey($id) {

		$this->load->model('key_cupones_models','dao');

		return $this->dao->getkey($id);

	}


	public function getCategorias() {

		$this->load->model('ccategoriawdsl_models','daocate');

		return $this->daocate->get();

	}

	public function getProductosverificarcateg($id,$idcate) {

		$this->load->model('cproductoswdsl_models','dao');

		return $this->dao->getProductosverificarcateg($id,$idcate);


	}


	public function getProductosverificar($id,$referencia) {

		$this->load->model('cproductoswdsl_models','dao');

		return $this->dao->getProductosverificar($id,$referencia);


	}

	
	public function getProductos($idCategoria) {

		$this->load->model('cproductoswdsl_models','dao');

		return $this->dao->getProductos($idCategoria);


	}

	public function editar($id,$datos) {

		$this->db->trans_begin();

		$this->load->model('cupones_wdsl_models','daoe');

		$this->daoe->update($id,$datos);

		logsError($this->db->error(),$this->class,'editar');
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return -1;
		} else {
			$this->db->trans_commit();

			return 1;
		}

	}
	public function update($datos,$idcupones_wdsl,$tipo,$productosOcategorias) {



		$this->load->model('cupones_wdsl_models','dao');
		$this->db->trans_begin();

		$this->dao->update($idcupones_wdsl,$datos);
		$this->load->model('cupones_pro_cate_models','daocp');

		//for para eliminar
		$resultado=$this->daocp->getProductosCatgeorias($idcupones_wdsl);
		foreach ($resultado->result() as $rowx) {
			$this->daocp->deletegetby($rowx->idcupones_pro_cate);
		}
		//se insetararan los nuevos
		if($tipo==1){

			foreach($productosOcategorias as $selected){
				$parametros = array(
					"idcupones_wdsl" => $idcupones_wdsl,
					"ProductReference" => $selected,
					"IdCategory" => NULL);


				
				$var=$this->daocp->verificarProducto($idcupones_wdsl,$selected);

				if($var==0){

					$this->daocp->insertar($parametros);
				}
			}
		}else{


			foreach($productosOcategorias as $selected){
				$parametros = array(
					"idcupones_wdsl" => $idcupones_wdsl,
					"ProductReference" => NULL,
					"IdCategory" => $selected);

				

				$var=$this->daocp->verificarCategoria($idcupones_wdsl,$selected);

				if($var==0){

					$this->daocp->insertar($parametros);
				}
			}

		}


		logsError($this->db->error(),$this->class,'update');
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return -1;
		} else {
			$this->db->trans_commit();

			return 1;
		}



		

	}

	public function guardar($tipo,$datos,$productosOcategorias) {

		$this->load->model('cupones_wdsl_models','dao');
		$this->load->model('cupones_pro_cate_models','daocp');
		$this->load->model('key_cupones_models','daok');
		$this->db->trans_begin();

		$idcupones_wdsl=$this->dao->insertar($datos);

		$datosinfo =(Object)$datos;

		//$can=$datosinfo->cantidad*2;

		for($i=0;$i<$datosinfo->cantidad;$i++){

			$prefijo=$datosinfo->prefijo;

			$string = bin2hex(openssl_random_pseudo_bytes(6));

			$parametros = array(
				"estado" => 1,
				"idcupones_wdsl" => $idcupones_wdsl,
				"idkey_cupones" => strtoupper ($prefijo.$string));

			$this->daok->insertar($parametros);


		}


		
		if($tipo==1){

			foreach($productosOcategorias as $selected){
				$parametros = array(
					"idcupones_wdsl" => $idcupones_wdsl,
					"ProductReference" => $selected,
					"IdCategory" => NULL);

				$this->daocp->insertar($parametros);
			}
		}else{

			foreach($productosOcategorias as $selected){
				$parametros = array(
					"idcupones_wdsl" => $idcupones_wdsl,
					"ProductReference" => NULL,
					"IdCategory" => $selected);

				$this->daocp->insertar($parametros);
			}

		}

		logsError($this->db->error(),$this->class,'guardar');
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return -1;
		} else {
			$this->db->trans_commit();

			return 1;
		}



		

	}

	
	

	
}
?>
