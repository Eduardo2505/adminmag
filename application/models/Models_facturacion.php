<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Models_facturacion extends CI_Model {

	function __construct() {
		parent::__construct();
		$this->load->database();
	}

	
	function insertar($data) {
		$this->db->trans_begin();
		$this->db->insert('facturacion', $data);
		$id=$this->db->insert_id();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return 0;
		}  else {
			$this->db->trans_commit();
			return $id;
		}
	} 

	function updateip($id, $data) {
		$this->db->trans_begin();
		$this->db->where('ip', $id);
		$this->db->update('facturacion', $data);
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}
	}

	function update($id, $data) {
		$this->db->trans_begin();
		$this->db->where('idfacturacion', $id);
		$this->db->update('facturacion', $data);
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}
	}

	function getbyid($idfacturacion) {


		$this->db->where('idfacturacion',$idfacturacion); 

		$query = $this->db->get('facturacion');

		return $query;
	}
	
	function get($idusuario,$ip) {

		// echo $idusuario."<br>";
		// echo $ip."<br>";
		$this->db->where('idusuario',$idusuario); 
		$this->db->where('referenceCode IS NULL', null, false); 
		$query = $this->db->get('facturacion');
		//$row = $query->row();
		//$row->Nombre
		return $query;
	}

	function getuser($idusuario) {
		$this->db->where('idusuario',$idusuario); 
		$this->db->where('transactionId!=',null); 
		$query = $this->db->get('facturacion');
		return $query;
	}
	

	


}