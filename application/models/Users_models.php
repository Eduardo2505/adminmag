<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');
class Users_models extends CI_Model {
	function __construct() {
		parent::__construct();
		$this->load->database();
	}

public function login($email,$password) {

// $condition = "correo =" . "'" . $data['email'] . "' AND " . "password =" . "'" . $data['password'] . "'";
$this->db->select('*');
$this->db->from('admin');
// $this->db->where($condition);
$this->db->where('email',$email);
$this->db->where('password',$password);
//$this->db->where('idrolusuario',1);
$this->db->limit(1);
$query = $this->db->get();

if ($query->num_rows() == 1) {
return true;
} else {
return false;
}
}

public function read_user_information($email) {

// $condition = "correo =" . "'" . $email . "'";
$this->db->select('*');
$this->db->from('usuarios');
$this->db->where('correo',$email);
$this->db->limit(1);
$query = $this->db->get();

if ($query->num_rows() == 1) {
return $query->result();
} else {
return false;
}
}

public function emailusuariocompra($idcompra) {
echo $idcompra;
$this->db->select('correo');
$this->db->from('usuarios u');
$this->db->join('compras c','c.idusuario = u.idusuario');
$this->db->where('c.idcompra',$idcompra);
$this->db->limit(1);
$query = $this->db->get();
return $query->row()->correo;
}

}
