<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Models_historial extends CI_Model {

	function __construct() {
		parent::__construct();
		$this->load->database();

	}
	
	public function agregarPedido($data)
	{
		$this->db->trans_begin();
		$this->db->insert('historial', $data);
		$valor=$this->db->insert_id();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return false;
		}  else {
			$this->db->trans_commit();
			return true;
		}
	}
	
	public function getHistorial($idCliente)
	{
		/*$this->db->where('id_cliente',$idCliente);
		$this->db->order_by("id_pedido", "asc");
		$this->db->distinct('id_pedido');
		$query = $this->db->get('historial');
		return $query;*/
		$this->db->select('DISTINCT(id_pedido), fecha_compra, transaction_state'); 
		$this->db->where('id_cliente',$idCliente);
		$this->db->order_by("id_pedido", "asc");
		$query = $this->db->get('historial');
		return $query;
	}
	
	public function getPagosPendientes($idCliente)
	{
		$this->db->select('transactionId, idtransactionState'); 
		$this->db->where('idusuario',$idCliente);
		$this->db->where('idtransactionState',7);
		$this->db->order_by("idfacturacion", "asc");
		$query = $this->db->get('facturacion');
		return $query;
	}
	
	public function actualizarEstatusOrden($referenceCode, $data)
	{
		$this->db->trans_begin();
		$this->db->where('reference_code', $referenceCode);
		$this->db->update('historial', $data);
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}
		return $this->db->trans_status();
	}
	
	public function getMaxIdPedido()
	{
		$row = $this->db->query('SELECT MAX(id_pedido) AS `maxid` FROM `historial`')->row();
		if ($row) 
		{
			return intval($row->maxid) +1;
		}
		else
		{
			return 1;
		}
	}

}