<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Cupones_pro_cate_models extends CI_Model {

	function __construct() {
		parent::__construct();
		$this->load->database();
	}

	function delete() {
		
		$this->db->empty_table('cupones_pro_cate');
	}

	function insertar($data) {
		
		$this->db->insert('cupones_pro_cate', $data);
		
	}


	function update($idcupones_pro_cate, $data) {
		
		$this->db->where('idcupones_pro_cate', $idcupones_pro_cate);
		$this->db->update('cupones_pro_cate', $data);

	}

	function get($idCategoria) {
		$sql="";
		
		
		return $this->db->query($sql);
	}

	function deletegetby($idcupones_pro_cate) {
		$this->db->where('idcupones_pro_cate', $idcupones_pro_cate);
		$this->db->delete('cupones_pro_cate'); 
	}


	function getProductosCatgeorias($idcupones_wdsl) {
		$this->db->where('idcupones_wdsl',$idcupones_wdsl);  
		$query = $this->db->get('cupones_pro_cate');
		return $query;
	}


	function getId($idcupones_pro_cate) {
		$this->db->where('idcupones_pro_cate',$idcupones_pro_cate);  
		$query = $this->db->get('cupones_pro_cate');
		$row = $query->row();
		return $row;
	}

	function verificarProducto($idcupones_wdsl,$ProductReference) {

		
		$sql="SELECT 
		count(*) total
		FROM
		cupones_pro_cate 
		WHERE idcupones_wdsl=$idcupones_wdsl and ProductReference='$ProductReference'";	

		
		$query=$this->db->query($sql);
		$row = $query->row();
		return $row->total;
	}

	function verificarCategoria($idcupones_wdsl,$IdCategory) {
		$sql="SELECT 
		count(*) total
		FROM
		cupones_pro_cate 
		WHERE idcupones_wdsl=$idcupones_wdsl and IdCategory=$IdCategory";		

		$query=$this->db->query($sql);
		$row = $query->row();
		return $row->total;
	}


	


	

}