<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Region_wdsl_models extends CI_Model {

	function __construct() {
		parent::__construct();
		$this->load->database();
	}


	function delete() {
		
		$this->db->empty_table('region_wdsl');
	}

	function deletecomunas() {
		
		$this->db->empty_table('comunas_wdsl');
	}


	

	function insertar($data) {
		
		$this->db->insert('region_wdsl', $data);
		
	}


	function insertarcomunas($data) {
		
		$this->db->insert('comunas_wdsl', $data);
		
	}


	

	


	

}