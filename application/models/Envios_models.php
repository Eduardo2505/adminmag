<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Envios_models extends CI_Model {

	function __construct() {
		parent::__construct();
		$this->load->database();
	}

	function insertarCampaniaCate($data) {
		
		$this->db->insert('envios_cate_wdsl', $data);
		return $this->db->insert_id();
		
	}

	function deleteCampaniaCate($id) {
		
		$this->db->where('idcampania', $id);
		$this->db->delete('envios_cate_wdsl');

	}

	function deleteCampaniaProductid($id,$categoria) {
		$sql="DELETE FROM envios_productos_wdsl 
		WHERE
		ProductReference IN (SELECT 
		ProductReference
		FROM
		c_productos_categorias
		
		WHERE
		IdCategory =$categoria)
		AND idcampania = $id";
		
		return $this->db->query($sql);
	}

	function deleteCampaniaProduct($idcampania) {
		
		$this->db->where('idcampania', $idcampania);
		$this->db->delete('envios_productos_wdsl');

	}

	function insertarCampaniaProduct($data) {
		
		$this->db->insert('envios_productos_wdsl', $data);
		return $this->db->insert_id();
		
	}

	function getProvidenciascvs($ip,$id) {
		
		//$this->db->where('ip',$ip);  
		$this->db->where('id',$id); 
		$query = $this->db->get('campania_regiones_wdsl');
		$response = $query->result_array();

		return $response;
	}

	function getProvidencias($ip,$id) {
		
		//$this->db->where('ip',$ip);  
		$this->db->where('id',$id); 
		$query = $this->db->get('campania_regiones_wdsl');
		return $query;
	}

	function deleteregion($id) {
		
		$this->db->where('idcampania_regiones_wdsl', $id);
		$this->db->delete('campania_regiones_wdsl');

	}


	function updateregion($id, $data) {
		
		$this->db->where('idcampania_regiones_wdsl', $id);
		$this->db->update('campania_regiones_wdsl', $data);

	}


	function insertarregionCampania($data) {
		
		$this->db->insert('campania_regiones_wdsl', $data);
		return $this->db->insert_id();
		
	}
	

	function insertar($data) {
		
		$this->db->insert('campania_envios_wdsl', $data);
		return $this->db->insert_id();
		
	}

	function update($id, $data) {
		
		$this->db->where('id', $id);
		$this->db->update('campania_envios_wdsl', $data);

	}

	function getId($id) {
		$this->db->where('id',$id);  
		$query = $this->db->get('campania_envios_wdsl');
		$row = $query->row();
		return $row;
	}

	
	function getcampanias($offset,$limite,$filtro) {
		$sql="SELECT 
		id,
		campania,
		registro,
		IF(estatus = 1, 'Activo', 'Inactivo') estado
		FROM
		campania_envios_wdsl 
		WHERE
		CONCAT(id,
		' ',
		campania,
		' ',
		registro,
		' ') LIKE '%$filtro%'
		ORDER BY registro
		LIMIT $offset , $limite";
		
		return $this->db->query($sql);
	}

	function getcampaniascout($filtro) {
		$sql="SELECT 
		count(*) total
		FROM
		campania_envios_wdsl 
		WHERE
		CONCAT(id,
		' ',
		campania,
		' ',
		registro,
		' ') LIKE '%$filtro%'";		
		$query=$this->db->query($sql);
		$row = $query->row();
		return $row->total;
	}

	function vertificaComuna($idcomunas,$idregiones,$idcampania) {
		$sql="SELECT 
		count(*) total
		FROM
		campania_regiones_wdsl 
		WHERE idProvince=$idcomunas and idState=$idregiones and id=$idcampania";		
		$query=$this->db->query($sql);
		$row = $query->row();
		return $row->total;
	}



	


	

}