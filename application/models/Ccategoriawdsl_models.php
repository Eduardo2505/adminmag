<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Ccategoriawdsl_models extends CI_Model {

	function __construct() {
		parent::__construct();
		$this->load->database();
	}

	function getCategoriaCampania($idcampania) {
		$sql="SELECT c.IdCategory,c.NameCategory,
		(SELECT 
		COUNT(*)
		FROM
		db_impreya.envios_cate_wdsl
		WHERE
		idcampania = $idcampania AND idCategory = c.IdCategory) validar
		FROM c_categorias_wdsl c";
		
		return $this->db->query($sql);
	}


	function deletecp() {
		
		
		$this->db->truncate('c_productos_categorias'); 
	}
	

	function delete() {
		$this->db->truncate('c_categorias_wdsl');
		
	}

	function insertar($data) {
		
		$this->db->insert('c_categorias_wdsl', $data);
		
	}


	function insertarpc($data) {
		
		$this->db->insert('c_productos_categorias', $data);
		
	}



	function update($id, $data) {
		$this->db->trans_begin();
		$this->db->where('IdCategory', $id);
		$this->db->update('c_categorias_wdsl', $data);
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}
	}

	function updatestado($estado) {

		$data = array('estado'=>$estado);
		$this->db->update('c_categorias_wdsl', $data);
	
	}

	function updatewd($IdCategory,$idws) {

		$sql="UPDATE c_categorias_wdsl t SET t.id_wp = $idws WHERE t.IdCategory = $IdCategory";

	    $this->db->query($sql);
	}

	function get() {
		$query = $this->db->get('c_categorias_wdsl');
		return $query;
	}

	function getId($id) {
		$this->db->where('IdCategory',$id);  
		$query = $this->db->get('c_categoria_wdsl');
		$row = $query->row();
		return $row;
	}

	function getCategorias($status) {
		$this->db->where('estado',$status);  
		$this->db->order_by('IdCategory', 'DESC');
		$query = $this->db->get('c_categorias_wdsl');
		return $query;
	}

	function getIdCategory($id) {

		$sql="SELECT COUNT(*) total FROM c_categorias_wdsl WHERE  IdCategory=$id";
		$query= $this->db->query($sql);
		$row = $query->row();
		return $row->total;;
	}

	


	

}