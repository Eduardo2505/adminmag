<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Cproductoswdsl_models extends CI_Model {

	function __construct() {
		parent::__construct();
		$this->load->database();
	}

	function getProductosCampania($idCategoria,$idcampania) {
		$sql="SELECT DISTINCT
		p.Name, p.ProductReference,
		(select count(*) from envios_productos_wdsl where ProductReference=p.ProductReference and idcampania=$idcampania) validar
		FROM
		c_productos_categorias cp
		INNER JOIN
		c_productos_wdsl p ON p.ProductReference = cp.ProductReference
		INNER JOIN
		c_categorias_wdsl c ON c.IdCategory = cp.IdCategory";
		if($idCategoria!=-1){
			$sql.=" where c.IdCategory=$idCategoria ";
		}  
		
		return $this->db->query($sql);
	}

	function delete() {
		
		$this->db->truncate('c_productos_wdsl');
	}

	function insertarImgProd($data) {
		
		$this->db->insert('c_imagenes', $data);
		
	}
	function deleteImgProd($ProductReference) {
		
		$this->db->where('ProductReference', $ProductReference);
        $this->db->delete('c_imagenes');
		
	}

	function getImagenes($ProductReference) {
		
		$this->db->where('ProductReference', $ProductReference);
		$query = $this->db->get('c_imagenes');
		return $query;
		
	} 
	
	
	

	function insertar($data) {
		
		$this->db->insert('c_productos_wdsl', $data);
		
	}

	function crontab($estado) {

		$data = array('estado'=>$estado);
		$this->db->update('crontab', $data);
	
	}
	function crontabEstado() {

		$sql="SELECT 
		estado
		FROM
		crontab";
		$query=$this->db->query($sql);
		$row = $query->row();
		return $row->estado;
	}

	function updatestado($estado) {

		$data = array('estado'=>$estado);
		$this->db->update('c_productos_wdsl', $data);
	
	}


	function update($ProductReference, $data) {
		$this->db->trans_begin();
		$this->db->where('ProductReference', $ProductReference);
		$this->db->update('c_productos_wdsl', $data);
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}
	}

	function getProductosverificarcateg($id,$cate) {

		$sql="SELECT 
		count(*) total
		FROM
		cupones_pro_cate pc
		where  pc.idcupones_wdsl=$id and pc.IdCategory=$cate";
		
		$query=$this->db->query($sql);
		$row = $query->row();
		return $row->total;
	}


	function getProductosverificar($id,$ref) {

		$sql="SELECT 
		count(*) total
		FROM
		cupones_pro_cate pc
		where  pc.idcupones_wdsl=$id and pc.ProductReference='$ref'";
		
		$query=$this->db->query($sql);
		$row = $query->row();
		return $row->total;
	}

	function getProductos($idCategoria) {
		$sql="SELECT 
		DISTINCT p.Name,p.ProductReference
		FROM
		c_productos_categorias cp
		INNER JOIN
		c_productos_wdsl p ON p.ProductReference = cp.ProductReference
		inner join c_categorias_wdsl c on c.IdCategory=cp.IdCategory";
		if($idCategoria!=-1){
			$sql.=" where c.IdCategory=$idCategoria ";
		}  
		
		return $this->db->query($sql);
	}

	function get() {
		$query = $this->db->get('c_productos_wdsl');
		return $query;
	}

	function getId($ProductReference) {
		$this->db->where('ProductReference',$ProductReference);  
		$query = $this->db->get('c_productos_wdsl');
		$row = $query->row();
		return $row;
	}


	function getreferencia($id) {

		$sql="SELECT COUNT(*) total FROM c_productos_wdsl WHERE  ProductReference='$id'";
		$query= $this->db->query($sql);
		$row = $query->row();
		return $row->total;;
	}

	function getProductosestadocount($status) {
		
		$sql="SELECT COUNT(*) total
				FROM c_productos_wdsl p
			INNER JOIN c_productos_categorias cpc on p.ProductReference = cpc.ProductReference
			INNER JOIN c_categorias_wdsl ccw on cpc.IdCategory = ccw.IdCategory
			where p.estado=$status";
            $query= $this->db->query($sql);
			$row = $query->row();
			return $row->total;
	}

	

	function getProductosestado($status,$offset,$limit) {

		$sql="SELECT p.ProductReference as productReference
					,p.id_wp idproductowp
				,ccw.id_wp idcategoriawp
				,p.url_img
				,p.Name as namepro
				, p.descripcion
				,p.precio
				FROM c_productos_wdsl p
			INNER JOIN c_productos_categorias cpc on p.ProductReference = cpc.ProductReference
			INNER JOIN c_categorias_wdsl ccw on cpc.IdCategory = ccw.IdCategory
			where p.estado=$status order by p.ProductReference desc limit $offset,$limit";
       // echo $sql."\n";
		return  $this->db->query($sql);
	}
	

	function updatewd($ProductReference,$idws) {

		$sql="UPDATE c_productos_wdsl t SET t.id_wp = $idws WHERE t.ProductReference ='$ProductReference'";
		echo $sql."\n";
		$data = array('ProductReference'=>$ProductReference);
		$this->db->where('id_wp',$idws);
		$this->db->update('c_productos_wdsl', $data);

	}


	function update_batch($data) {
		$this->db->trans_begin();
		$this->db->update_batch('c_productos_wdsl', $data, 'ProductReference');
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}

		
	}


	

}