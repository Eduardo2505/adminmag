<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Models_ventas_wsdl extends CI_Model {

	function __construct() {
		parent::__construct();
		$this->load->database();

	}


	function geTcostoenvia($idState) {

		$SQl="SELECT 
    r.*
FROM
    campania_regiones_wdsl r
        INNER JOIN
    campania_envios_wdsl ce ON ce.id = r.id
WHERE
    ce.estatus = 1 and r.idProvince=$idState limit 1";

		return $this->db->query($SQl);
	}

	function getDescuentokeyproducto($key_cupon,$ProductReference) {

		$SQl = "SELECT 
		c.des_valor_tipo,
		c.valor_des_tipo,
		IF(IFNULL(c.infinito, - 1) != - 1, 999999999, c.cantidad_utilizadas) disponibles,
		(SELECT COUNT(*)
		FROM ventas_wsdl
		WHERE
		key_cupon = '$key_cupon') utilizados,
		c.referencia,
		k.idkey_cupones,
		cpc.ProductReference
		FROM
		key_cupones k
		INNER JOIN
		cupones_wdsl c ON c.idcupones_wdsl = k.idcupones_wdsl
		INNER JOIN
		cupones_pro_cate cpc ON cpc.idcupones_wdsl = k.idcupones_wdsl
		WHERE
		c.tipo = 1 
		AND k.estado = 1
		AND c.estado = 1
		AND c.des_valor_tipo!=-1
		AND k.idkey_cupones='$key_cupon'
		AND cpc.ProductReference='$ProductReference'
		AND CURDATE() BETWEEN c.fecha_inicio AND c.fecha_final";


		return $this->db->query($SQl);
	}
	function getDescuentokeyCategoria($key_cupon,$ProductReference) {

		$SQl = "SELECT 
		c.des_valor_tipo,
		c.valor_des_tipo,
		IF(IFNULL(c.infinito, - 1) != - 1, 999999999, c.cantidad_utilizadas) disponibles,
		(SELECT COUNT(*)
		FROM ventas_wsdl
		WHERE
		key_cupon = '$key_cupon') utilizados,
		c.referencia,
		k.idkey_cupones,
		cpct.ProductReference
		FROM
		key_cupones k
		INNER JOIN cupones_wdsl c ON c.idcupones_wdsl = k.idcupones_wdsl
		INNER JOIN cupones_pro_cate cpc ON cpc.idcupones_wdsl = k.idcupones_wdsl    
		INNER JOIN c_productos_categorias  cpct on cpct.IdCategory=cpc.IdCategory
		WHERE
		k.idkey_cupones = '$key_cupon'
		AND cpct.ProductReference ='$ProductReference'
		AND c.tipo = 2
		AND k.estado = 1
		AND c.estado = 1
		AND c.des_valor_tipo!=-1
		AND CURDATE() BETWEEN c.fecha_inicio AND c.fecha_final";
		$query = $this->db->query($SQl);
		

		return $query;
	}

	public function deleteiventas($idventas_wsdl)
	{
		$this->db->trans_begin();
		$this->db->where('idventas_wsdl', $idventas_wsdl);
		$this->db->delete('ventas_wsdl');
		$valor=$this->db->insert_id();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return false;
		}  else {
			$this->db->trans_commit();
			return true;
		}
	}


	public function delete($id_proyecto)
	{
		$this->db->trans_begin();
		$this->db->where('id_proyecto', $id_proyecto);
		$this->db->delete('ventas_wsdl');
		$valor=$this->db->insert_id();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return false;
		}  else {
			$this->db->trans_commit();
			return true;
		}
	}
	
	public function insertar($data)
	{
		$this->db->trans_begin();
		$this->db->insert('ventas_wsdl', $data);
		$valor=$this->db->insert_id();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return false;
		}  else {
			$this->db->trans_commit();
			return true;
		}
	}

	function getVentasfacOrder($idfacturacion) {

		
		$this->db->where('idfacturacion',$idfacturacion);  
		$this->db->where('ip!=','OK'); 
		$query = $this->db->get('ventas_wsdl');
		return $query;
	}

	function getVentasfac($idfacturacion) {

		
		$this->db->where('idfacturacion',$idfacturacion);   
		$query = $this->db->get('ventas_wsdl');
		return $query;
	}


	function getVentas($customerID,$ip,$idfacturacion) {

		

		$this->db->select('v.*');
		$this->db->from('ventas_wsdl v');
		$this->db->where('f.referenceCode IS NULL', null, false); 
		$this->db->join('facturacion f', 'f.idfacturacion = v.idfacturacion');
        $query = $this->db->get();
		return  $query;
	}

	function getVerificar($customerID,$id_proyecto,$referencia) {
		$this->db->where('customerID',$customerID);  
		$this->db->where('id_proyecto',$id_proyecto); 
		$this->db->where('referencia',$referencia);   
		$query = $this->db->get('ventas_wsdl');
		return $query->num_rows();
	}

	public function update($id_proyecto,$data)
	{
		$this->db->trans_begin();
		//$this->db->where('customerID',$customerID);  
		$this->db->where('id_proyecto',$id_proyecto); 
	//	$this->db->where('referencia',$referencia); 
		$this->db->update('ventas_wsdl', $data);
		$valor=$this->db->insert_id();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return false;
		}  else {
			$this->db->trans_commit();
			return true;
		}
	}


	public function updateidventa($idventas_wsdl,$data)
	{
		$this->db->trans_begin(); 
		$this->db->where('idventas_wsdl',$idventas_wsdl); 
		$this->db->update('ventas_wsdl', $data);
		$valor=$this->db->insert_id();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return false;
		}  else {
			$this->db->trans_commit();
			return true;
		}
	}


	public function updateidorder($idorder,$data)
	{
		$this->db->trans_begin(); 
		$this->db->where('orderID',$idorder); 
		$this->db->update('ventas_wsdl', $data);
		$valor=$this->db->insert_id();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return false;
		}  else {
			$this->db->trans_commit();
			return true;
		}
	}
	
	

}