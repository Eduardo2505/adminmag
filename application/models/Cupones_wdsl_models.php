<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Cupones_wdsl_models extends CI_Model {

	function __construct() {
		parent::__construct();
		$this->load->database();
	}

	function delete() {
		
		$this->db->empty_table('cupones_wdsl');
	}

	function insertar($data) {
		
		$this->db->insert('cupones_wdsl', $data);
		return $this->db->insert_id();
		
	}


	function update($idcupones_wdsl, $data) {
		
		$this->db->where('idcupones_wdsl', $idcupones_wdsl);
		$this->db->update('cupones_wdsl', $data);

	}

	function getProducts($id,$tipo) {
		$sql="";
		if($tipo==2){
			$sql="SELECT 
			pc.IdCategory idp, c.NameCategory des
			FROM
			cupones_pro_cate pc
			INNER JOIN
			c_categorias_wdsl c ON c.IdCategory = pc.IdCategory
			where  pc.idcupones_wdsl=$id order by pc.IdCategory";
			
		}else{

			$sql="SELECT 
			pc.ProductReference idp, c.Name des
			FROM
			cupones_pro_cate pc
			INNER JOIN
			c_productos_wdsl c ON c.ProductReference = pc.ProductReference
			where  pc.idcupones_wdsl=$id order by pc.ProductReference";

		}
		
		return $this->db->query($sql);
	}

	function getkeybus($offset,$limite,$filtro) {
		$sql="SELECT 
		k.idkey_cupones,
		IF(k.estado = 1, 'Libre', 'Utilizado') estado,
		k.cliente,
		cw.campania,
		cw.referencia,
		k.registro
		FROM
		key_cupones k
		INNER JOIN
		cupones_wdsl cw ON k.idcupones_wdsl = cw.idcupones_wdsl
		WHERE
		CONCAT(cw.campania,
		' ',
		cw.referencia,
		' ',
		k.registro,
		' ',
		k.idkey_cupones,
		' ',
		IF(k.cliente IS NULL, '', k.cliente),
		' ',
		IF(k.estado = 1, 'Libre', 'Utilizado'),
		' ') LIKE '%$filtro%'
		ORDER BY k.registro
		LIMIT $offset , $limite";
		
		return $this->db->query($sql);
	}

	function getkeybuscount($filtro) {
		$sql="SELECT 
		count(*) total
		FROM
		key_cupones k
		INNER JOIN
		cupones_wdsl cw ON k.idcupones_wdsl = cw.idcupones_wdsl
		WHERE
		CONCAT(cw.campania,
		' ',
		cw.referencia,
		' ',
		k.registro,
		' ',
		k.idkey_cupones,
		' ',
		IF(k.cliente IS NULL, '', k.cliente),
		' ',
		IF(k.estado = 1, 'Libre', 'Utilizado'),
		' ') LIKE '%$filtro%'";		
		$query=$this->db->query($sql);
		$row = $query->row();
		return $row->total;
	}


	function getCampanias($offset,$limite,$filtro) {
		$sql="SELECT 
		idcupones_wdsl,
		referencia,
		campania,
		fecha_inicio,
		fecha_final,
		registro,
		IF(estado=1, 'Activo', 'Inactivo') estado
		FROM
		cupones_wdsl
		WHERE
		CONCAT(referencia,
		' ',
		campania,
		' ',
		fecha_inicio,
		' ',
		fecha_final,
		' ') LIKE '%$filtro%'
		ORDER BY registro
		LIMIT $offset , $limite";
		
		return $this->db->query($sql);
	}

	function getCampaniascount($filtro) {
		$sql="SELECT 
		count(*) total
		FROM
		cupones_wdsl
		WHERE
		CONCAT(referencia,
		' ',
		campania,
		' ',
		fecha_inicio,
		' ',
		fecha_final,
		' ') LIKE '%$filtro%' ";		
		$query=$this->db->query($sql);
		$row = $query->row();
		return $row->total;
	}


	function getId($idcupones_wdsl) {
		$this->db->where('idcupones_wdsl',$idcupones_wdsl);  
		$query = $this->db->get('cupones_wdsl');
		$row = $query->row();
		return $row;
	}

	


	

}