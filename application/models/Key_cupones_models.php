<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Key_cupones_models extends CI_Model {

	function __construct() {
		parent::__construct();
		$this->load->database();
	}

	function delete() {
		
		$this->db->empty_table('key_cupones');
	}

	function insertar($data) {
		
		$this->db->insert('key_cupones', $data);
		
	}


	function update($idkey_cupones, $data) {
		
		$this->db->where('idkey_cupones', $idkey_cupones);
		$this->db->update('key_cupones', $data);
		
	}

	function getkey($id) {
		$sql="SELECT 
		k.idkey_cupones,
		k.cliente,
		k.registro,
		IF(k.estado = 1, 'Libre', 'Ocupado') estado,
		c.referencia,
		c.campania
		FROM
		key_cupones k
		INNER JOIN
		cupones_wdsl c ON c.idcupones_wdsl = k.idcupones_wdsl where c.idcupones_wdsl=$id";
		
		return $this->db->query($sql);

	}


	function getId($idkey_cupones) {
		$this->db->where('idkey_cupones',$idkey_cupones);  
		$query = $this->db->get('key_cupones');
		$row = $query->row();
		return $row;
	}

	


	

}