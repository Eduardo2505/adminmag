<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Models_cupones extends CI_Model {

	function __construct() {
		parent::__construct();
		$this->load->database();

	}

	function cuponEnvio($key_cupon) {

		$SQl = "SELECT 
    c.des_envio_tipo,
    c.valor_des_envio,
    IF(IFNULL(c.infinito, - 1) != - 1,
        999999999,
        c.cantidad_utilizadas) disponibles,
    (SELECT 
            COUNT(*)
        FROM
            ventas_wsdl
        WHERE
            key_cupon = '$key_cupon') utilizados,
    c.referencia,
    k.idkey_cupones
FROM
    key_cupones k
        INNER JOIN
    cupones_wdsl c ON c.idcupones_wdsl = k.idcupones_wdsl
WHERE
    k.estado = 1 AND c.estado = 1
        AND c.des_envio_tipo != - 1
        AND k.idkey_cupones = '$key_cupon'
        AND CURDATE() BETWEEN c.fecha_inicio AND c.fecha_final";


		return $this->db->query($SQl);
	}


	function login($usr,$password) {
		$this->db->where('usuario',$usr); 
		$this->db->where('contrasenia',$password); 
		$this->db->where('estatus',1);
		$query = $this->db->get('usuario');
		return $query;
	}
	
	public function agregarCupon($data)
	{
		$this->db->trans_begin();
		$this->db->insert('cupones', $data);
		$valor=$this->db->insert_id();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return 0;
		}  else {
			$this->db->trans_commit();
			return $valor;
		}
	}
	
	public function getCupones()
	{
		$this->db->where('estatus',1);
		$this->db->order_by("id_cupon", "asc");
		$query = $this->db->get('cupones');
		return $query;
	}
	
	function deleteCupon($id)
	{
		$this->db->where('id_cupon', $id);
		$this->db->delete('cupones');   
	}

}